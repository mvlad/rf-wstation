#include <avr/io.h>
#include <util/delay.h>

#include "sys/assert.h"
#include "sys/i2c.h"

#include "app/bmp085.h"

static int16_t ac1, ac2, ac3;
static int16_t b1, b2;
static int16_t mb, mc, md;
static uint16_t ac4, ac5, ac6;
static uint8_t oversampling;

static uint8_t
bmp_read(uint8_t a)
{
        uint8_t r;

        i2c_start();
        i2c_write(BMP_READY_TO_WRITE);
        i2c_write(a);
        i2c_stop();

        i2c_start();
        i2c_write(BMP_READY_TO_READ);
        r = i2c_read();
        i2c_stop();

        return r;
}

static uint16_t
bmp_read16(uint8_t a)
{
        uint16_t d;

        i2c_start();
        i2c_write(BMP_READY_TO_WRITE);
        i2c_write(a);
        i2c_stop();

        i2c_start();
        i2c_write(BMP_READY_TO_READ);

        /* read MSB */
        d = i2c_read();
        d <<= 8;

        /* read LSB */
        d |= i2c_read();
        i2c_stop();

        return d;
}

static void
bmp_write(uint8_t a, uint8_t b)
{
        i2c_start();

        i2c_write(BMP_READY_TO_WRITE);
        i2c_write(a);
        i2c_write(b);

        i2c_stop();
}

static uint16_t
bmp_read_temp(void)
{
        uint16_t d;

        bmp_write(BMP_CONTROL, BMP_READTEMPCMD);

        _delay_ms(5);


        i2c_start();
        i2c_write(BMP_READY_TO_WRITE);
        i2c_write(0xf6);
        i2c_stop();

        i2c_start();
        i2c_write(BMP_READY_TO_READ);

        d = i2c_read();
        d <<= 8;

        i2c_stop();

        i2c_start();
        i2c_write(BMP_READY_TO_WRITE);
        i2c_write(0xf7);
        i2c_stop();

        i2c_start();
        i2c_write(BMP_READY_TO_READ);

        d |= i2c_read();
        i2c_stop();

        return d;
}

static int32_t
computeB5(int32_t UT) 
{
        int32_t x1 = (UT - (int32_t) ac6) * ((int32_t) ac5) >> 15;
        int32_t x2 = ((int32_t) mc << 11) / (x1 + (int32_t) md);

        return x1 + x2;
}

static uint32_t
bmp_read_pressure(void)
{

        uint32_t raw;

        bmp_write(BMP_CONTROL, 
                        BMP_READPRESSURECMD + (oversampling << 6));

        switch (oversampling) {
        case BMP_ULTRALOWPOWER:
                _delay_ms(5);
                break;
        case BMP_STANDARD:
                _delay_ms(8);
                break;
        case BMP_HIGHRES:
                _delay_ms(14);
                break;
        default:
                _delay_ms(26);
                break;
        }

        raw = bmp_read16(BMP_PRESSUREDATA);

        raw <<= 8;
        raw |= bmp_read(BMP_PRESSUREDATA + 2);
        raw >>= (8 - oversampling);

        return raw;
}

int32_t
bmp_get_pressure(void)
{
        int32_t UT, UP; 
        int32_t B3, B5, B6;
        int32_t X1, X2, X3; 
        int32_t p;

        uint32_t B4, B7;

        UT = bmp_read_temp();
        UP = bmp_read_pressure();

        B5 = computeB5(UT);

        /* do pressure calcs */
        B6 = B5 - 4000;

        X1 = ((int32_t) b2 * ((B6 * B6) >>12)) >> 11;
        X2 = ((int32_t) ac2 * B6) >> 11;
        X3 = X1 + X2;

        B3 = ((((int32_t) ac1 * 4 + X3) << oversampling) + 2) / 4;


        X1 = ((int32_t) ac3 * B6) >> 13;
        X2 = ((int32_t) b1 * ((B6 * B6) >> 12)) >> 16;
        X3 = ((X1 + X2) + 2) >> 2;

        B4 = ((uint32_t) ac4 * (uint32_t)(X3 + 32768)) >> 15;
        B7 = ((uint32_t) UP - B3) * (uint32_t)(50000UL >> oversampling);


        if (B7 < 0x80000000) {
                p = (B7 * 2) / B4;
        } else {
                p = (B7 / B4) * 2;
        }

        X1 = (p >> 8) * (p >> 8);
        X1 = (X1 * 3038) >> 16;
        X2 = (-7357 * p) >> 16;


        p = p + ((X1 + X2 + (int32_t) 3791) >> 4);

        return p;
}

int32_t
bmp_get_temp(void) 
{
        /* following ds convention */
        int32_t B5, temp;
        int32_t UT;

        UT = bmp_read_temp();

        B5 = computeB5(UT);
        temp = (B5 + 8) >> 4;

        return temp;
}

void
bmp_get_all(volatile struct bmp_data *data)
{
        int32_t UT, UP; 
        int32_t B3, B5, B6;
        int32_t X1, X2, X3; 
        int32_t p;
        int32_t temp;

        uint32_t B4, B7;

        UT = bmp_read_temp();
        UP = bmp_read_pressure();

        B5 = computeB5(UT);
        temp = (B5 + 8) >> 4;

        /* do pressure calcs */
        B6 = B5 - 4000;

        X1 = ((int32_t) b2 * ((B6 * B6) >>12)) >> 11;
        X2 = ((int32_t) ac2 * B6) >> 11;
        X3 = X1 + X2;

        B3 = ((((int32_t) ac1 * 4 + X3) << oversampling) + 2) / 4;


        X1 = ((int32_t) ac3 * B6) >> 13;
        X2 = ((int32_t) b1 * ((B6 * B6) >> 12)) >> 16;
        X3 = ((X1 + X2) + 2) >> 2;

        B4 = ((uint32_t) ac4 * (uint32_t)(X3 + 32768)) >> 15;
        B7 = ((uint32_t) UP - B3) * (uint32_t)(50000UL >> oversampling);


        if (B7 < 0x80000000) {
                p = (B7 * 2) / B4;
        } else {
                p = (B7 / B4) * 2;
        }

        X1 = (p >> 8) * (p >> 8);
        X1 = (X1 * 3038) >> 16;
        X2 = (-7357 * p) >> 16;


        p = p + ((X1 + X2 + (int32_t) 3791) >> 4);

        data->temp = temp;
        data->pressure = p;
}

void
bmp_init(int mode)
{
        assert(bmp_read(0xd0) == 0x55);

        oversampling = mode;

        /* read calibration data */
        ac1 = bmp_read16(BMP_CAL_AC1);
        ac2 = bmp_read16(BMP_CAL_AC2);
        ac3 = bmp_read16(BMP_CAL_AC3);
        ac4 = bmp_read16(BMP_CAL_AC4);
        ac5 = bmp_read16(BMP_CAL_AC5);
        ac6 = bmp_read16(BMP_CAL_AC6);

        _delay_ms(100);

        b1 = bmp_read16(BMP_CAL_B1);
        b2 = bmp_read16(BMP_CAL_B2);

        _delay_ms(100);

        mb = bmp_read16(BMP_CAL_MB);
        mc = bmp_read16(BMP_CAL_MC);
        md = bmp_read16(BMP_CAL_MD);

        _delay_ms(100);

        if (ac1 == 0x0 || ac2 == 0x0 || ac3 == 0x0 || 
            ac4 == 0x0 || ac5 == 0x0 || ac6 == 0x0) {
                assert(0);
        }
}
