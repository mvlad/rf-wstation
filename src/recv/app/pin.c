#include "stm32f10x_conf.h"

#include "app/pin.h"

void
board_pin_on(GPIO_TypeDef *GPIO_PORT, uint16_t led)
{
        GPIO_PORT->BSRR = led;
}

void
board_pin_off(GPIO_TypeDef *GPIO_PORT, uint16_t led)
{
        GPIO_PORT->BRR = led;
}

void
board_pin_toggle(GPIO_TypeDef *GPIO_PORT, uint16_t led)
{
        GPIO_PORT->ODR ^= led;
}

void
pin_init(GPIO_TypeDef *GPIO_PORT, enum pin_mode type, uint32_t clk, uint16_t led)
{
        GPIO_InitTypeDef gpio;

        /* init the clock */
        RCC_APB2PeriphClockCmd(clk, ENABLE);

        /* configure gpio */
        gpio.GPIO_Pin = led;
        if (type == INPUT) {
                gpio.GPIO_Mode = GPIO_Mode_IN_FLOATING;
        } else if (type == OUTPUT) {
                gpio.GPIO_Speed = GPIO_Speed_50MHz;
                gpio.GPIO_Mode = GPIO_Mode_Out_PP;
        }

        GPIO_Init(GPIO_PORT, &gpio);
}
