# RF Protocol 

## UART frames

The temperature and the pressure are `int32_t` types, requiring 4-byte of
storage.  The data is sent over serial line every second, alternating between
the temperature and the pressure. For more info see 
[main file of the transmitter](src/trans/app/main.c)

## Frame format

    [ready to send data][start byte 0][start byte 1][destination byte][data bytes][checksum byte]
           0xf0             0x8f            0xaa         0xa1            0xXX         0xYY

* ready to send byte -- synch byte, this byte never reaches the receiver
* header frame -- composed from 3 bytes: start byte 0, and start byte 1 plus a
a destination byte (in case there are multiple receivers)
* 4-byte data frame
* at the end a CRC8 byte, used by the receiver the determine if the
data was sent correct.

The overhead of the encoding is quite large: for sending 4-byte data frame
additional 3-byte header and 1-byte checksum is required.
                                            
## Receiveing data

The receiver is responsible from recovering the data bytes from the entire
frame, making sure it has received the header frame, then the data bytes and
finally the checksum.  At the end it computes the checksum over the entire
frame. If the checksum computed is not 0 (zero) the entire frame is discarded.


