#ifndef __RF_H
#define __RH_H

/*
 * protocol for sending data over RF using serial:
 *
 * sending 4 bytes for 1 byte of data
 *
 * [ready to send data][start of byte 0][start byte 1][destination byte][data bytes][checksum byte]
 *      0xf0                0x8f            0xaa            0xa1            0xXX          0xYY
 *
 */

#define RF_READY_TO_SEND        0xf0

#define RF_START0               0x8f
#define RF_START1               0xaa

#define RF_DST                  0xa1

#define RF_FRAME_HEADER_SIZE    3
#define RF_FRAME_CRC_SIZE       1

#define RF_FRAME_DATA_SIZE      1
#define RF_FRAME_DATA_SIZE_BYTE 4       /* sending 4-bytes */

#define RF_FRAME_SIZE           (RF_FRAME_HEADER_SIZE + RF_FRAME_DATA_SIZE + RF_FRAME_CRC_SIZE)
#define RF_FRAME_SIZE_BYTE      (RF_FRAME_HEADER_SIZE + RF_FRAME_DATA_SIZE_BYTE + RF_FRAME_CRC_SIZE)

extern void
rf_send_frame(uint8_t data, uint8_t ready);

void
rf_send_frame_bytes(uint8_t *data, uint8_t len, uint8_t ready);

extern void
rf_send(uint8_t *d, uint8_t len);

#endif /* __RF_H */
