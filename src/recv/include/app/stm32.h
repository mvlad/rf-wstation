#ifndef __STM32_OLIMEXINO_H
#define __STM32_OLIMEXINO_H

/* 72 MHz -> 72 cycles per microsecond. */
#define CYCLES_PER_MICROSECOND  72

/* Pin number for the built-in button. */
#define BOARD_BUTTON_PORT       GPIOC
#define BOARD_BUTTON_PIN        GPIO_Pin_9

/* Pin number and Port for the built-in LED. */
#define BOARD_LED_PORT          GPIOA
#define BOARD_LED_PIN           GPIO_Pin_5
#define BOARD_ERROR_LED_PIN     GPIO_Pin_1


#define CLOCK_SPEED_MHZ         CYCLES_PER_MICROSECOND
#define CLOCK_SPEED_HZ          (CLOCK_SPEED_MHZ * 1000000UL)
#define SYSTICK_RELOAD_VAL      (1000 * CYCLES_PER_MICROSECOND - 1)

#define USART1_GPIO              GPIOA
#define USART1_CLK               RCC_APB2Periph_USART1
#define USART1_GPIO_CLK          RCC_APB2Periph_GPIOA
#define USART1_TxPin             GPIO_Pin_9
#define USART1_RxPin             GPIO_Pin_10
#define USART1_IRQn              USART1_IRQn
#define USART1_IRQHandler        USART1_IRQHandler

#define USART2_GPIO              GPIOA
#define USART2_CLK               RCC_APB1Periph_USART2
#define USART2_GPIO_CLK          RCC_APB2Periph_GPIOA
#define USART2_TxPin             GPIO_Pin_2
#define USART2_RxPin             GPIO_Pin_3
#define USART2_IRQn              USART2_IRQn
#define USART2_IRQHandler        USART2_IRQHandler

#define STM32_DELAY_US_MULT     12

#endif /* __STM32_OLIMEXINO_H */
