#ifndef __PINS_H
#define __PINS_H

#define PIN_ON(G, P)    \
        PORT##G |= (1 << PIN##G##P)

#define PIN_OFF(G, P)   \
        PORT##G &= ~(1 << PIN##G##P)

#define toggle_PIN(G, P) \
        PORT##G ^= (1 << PIN##G##P)

#define PIN_OUTPUT(G, P) \
        DDR##G |= (1 << DD##G##P)

#define PIN_INPUT(G, P) \
        DDR##G &= ~(1 << DD##G##P); \
        PORT##G |= (1 << PORT##G##P)

extern void 
pins_clear(void);

extern void 
pins_init(void);

#endif
