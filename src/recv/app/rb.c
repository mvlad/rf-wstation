#include "sys/stm32f10x_conf.h"
#include "app/rb.h"

void
rb_init(struct rb *rb, uint16_t size, uint8_t *buf)
{
        rb->head = rb->tail = 0;
        rb->size = size - 1;
        rb->buf = buf;
}


uint16_t
rb_count(struct rb *rb)
{
        struct rb *arb = rb;
        int32_t size = arb->tail - arb->head;

        if (arb->tail < arb->head)
                size += arb->size + 1;

        return (uint16_t) size;
}

int
rb_is_full(struct rb *rb)
{
        if (rb->tail + 1 == rb->head)
                return 1;

        if (rb->tail == rb->size && rb->head == 0)
                return 1;

        return 0;
}

int
rb_is_empty(struct rb *rb)
{
        return (rb->head == rb->tail);
}

void
rb_insert(struct rb *rb, uint8_t v)
{
        rb->buf[rb->tail] = v;

        if (rb->tail == rb->size)
                rb->tail = 0;
        else
                rb->tail += 1;
}

uint8_t
rb_remove(struct rb *rb)
{
        uint8_t c;

        c = rb->buf[rb->head];

        if (rb->head == rb->size)
                rb->head = 0;
        else
                rb->head += 1;

        return c;
}

int16_t
rb_safe_remove(struct rb *rb)
{
        if (rb_is_empty(rb))
                return -1;
        else
                return rb_remove(rb);
}

int
rb_safe_insert(struct rb *rb, uint8_t v)
{
        if (rb_is_full(rb)) {
                return 0;
        }

        rb_insert(rb, v);
        return 1;
}

int
rb_push_insert(struct rb *rb, uint8_t v)
{
        int ret = -1;

        if (rb_is_full(rb)) {
                ret = rb_remove(rb);
        }

        rb_insert(rb, v);
        return ret;
}

void
rb_reset(struct rb *rb)
{
        rb->tail = rb->head;
}

void
dev_init(struct usart_dev *d, USART_TypeDef *usart, struct rb *rb)
{
        d->usart = usart;
        d->rb = rb;

        rb_init(d->rb, RX_BUF, d->raw);
}
