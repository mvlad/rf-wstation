#ifndef __ASSERT_H
#define __ASSERT_H

#include <avr/io.h>
#include <util/delay.h>
#include "sys/pins.h"

static inline void
_panic(void)
{
        PIN_OUTPUT(B, 5);
        for (;;) {
                toggle_PIN(B, 5);
                _delay_ms(100);
        }
}

#define assert(exp)     do {    \
        if (!(exp))             \
                _panic();       \
} while (0)
                
#endif /* __ASSERT_H */
