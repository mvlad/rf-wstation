#ifndef __I2C_H
#define __I2C_H

/* 
 * I2C status codes
 */
#define I2C_START               0x08
#define I2C_RESTART             0x10
#define I2C_ACK_RECV            0x18
#define I2C_NACK_RECV           0x20

#define I2C_DATA_ACK_RECV       0x28
#define I2C_DATA_NACK_RECV      0x30

extern void
i2c_write(uint8_t byte);

extern uint8_t
i2c_read(void);

extern void
i2c_start(void);

extern void
i2c_stop(void);

extern void
i2c_init(void);

extern void
i2c_disable(void);

#endif /* __I2C_H */
