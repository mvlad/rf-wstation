#ifndef __USART_H
#define __USART_H

extern void
usart_init(void);

extern int
usart_send_byte(uint8_t byte);

extern int
usart_send_ch(unsigned char ch);

extern uint8_t
usart_recv_ch(void);

extern void
usart_flush_rx(void);

#endif /* __USART_H */
