#include <stddef.h>

#include "stm32f10x.h"

extern void __libc_init_array(void);

extern int main(int, char **, char **);
extern void exit(int) __attribute__ ((noreturn, weak));

/* The linker must ensure that these are at least 4-byte aligned. */
extern char __data_start__, __data_end__;
extern char __bss_start__, __bss_end__;
extern char __text_start__, __text_end__;

/* a marker placed in linker script */
extern uint8_t *__rom__;

void __attribute__ ((noreturn))
start_c(void)
{
	int exit_code;
	int *src = (int *) __rom__;
	int *dst = (int *) &__data_start__;

	/* Initialize .data, if necessary. */
	if (src != dst) {
		int *end = (int *) &__data_end__;
		while (dst < end) {
			*dst++ = *src++;
		}
	}

	/* Zero .bss. */
	dst = (int *) &__bss_start__;
	while (dst < (int *) &__bss_end__) {
		*dst++ = 0;
	}

	/* Run initializers. */
	__libc_init_array();

        /* init board here */
        SystemInit();

	/* Jump to main. */
	exit_code = main(0, 0, 0);
	if (exit) {
		exit(exit_code);
	}

	/* If exit is NULL, make sure we don't return. */
	for (;;)
		continue;
}
