# Trainer board with ATMEGA328p

Directories:

    app         <-- main app
    include     <-- header files for app and sys
    Makefile
    sys         <-- simple library for various protocols, USART, SPI and TWI

[Avr-libc][3] together with binutils-avr provides all the start-up routines, interrupt
vectors and linker files. Just compile for the right MCU and it is ready to go.

There 3 major state machines: 

* the main one is handling displaying the data on the 7-segment display.
* in the 16-bit timer interrupt handler the data from the sensor is 
retrieved and send over UART. The timer is set-up to overflow every half a second.
Also the local time/clock is being updated and the ticks are incremented.
* a external interrupt handler to switch between two different working
mode: displaying the temperature and time, or setting up the time manually 
with some additional buttons attached.


## 7-segment display

The transmitter uses the main loop to display current data information on the
[7-segment display][2]. The display is capable of using different kind of interfaces.
I've used its SPI interface because I2C is required for the temperature sensor
and the serial for RF. A rather crude polling-implementation is used and is
implemented in [spi](src/trans/sys/spi.c). The data sheet explains that the data
must be sent in a 4-byte frame, while some special command bytes (like the
brightness, cursor position) are used to control its functionality. 
See [7-segment-spi](src/trans/app/7segment.c) and 
[its header](src/trans/include/app/7segment.h) for more information.

### External interrupts

The external interrupt has attached a button to manually switch between 
two working modes:
* show the current time and the temperature.
* configure the local time.

### Timer interrupts

ATMEGA328p has one 16-bit timer (`TIM1`) and two 8-bit counters (`TIM0` and
`TIM2`). A pre-computed table with prescaler and timer values 
can be found in [timer](src/trans/sys/timer.c) sources.

* In `TIM1` interrupt handler the temperature and pressure is retrieved using the
I2C protocol, using a polling implementation in [i2c](src/trans/sys/i2c.c). 
* After that the we pack the data into a 4-byte data frame and send it over
UART.
* serves the functionality of a sys tick interrupt handler, keeping track of
current time.

### Retrieving temperature and pressure

While the TWI interface (I2C) is used as a communication protocol, the
[BMP085][1] data sheet describes how to retrieve and compute from raw data to
meaningful values.

[1]: https://www.adafruit.com/datasheets/BMP085_DataSheet_Rev.1.0_01July2008.pdf
[2]: https://www.sparkfun.com/datasheets/Components/LED/7-Segment/SFE-0012-DS-7segmentSerial-v41.pdf
[3]: http://www.atmel.com/webdoc/AVRLibcReferenceManual/index.html
