#include <sys/stat.h>
#include <errno.h>
#include <stddef.h>

#include "stm32f10x_conf.h"

#include "app/rb.h"
#include "app/usart.h"
#include "app/compiler.h"


#ifndef CONFIG_HEAP_START
extern char __heap_start;
#define CONFIG_HEAP_START               ((void *) &__heap_start)
#endif

#ifndef CONFIG_HEAP_END
extern char __heap_end;
#define CONFIG_HEAP_END                 ((void *) &__heap_end)
#endif

/*
 * _sbrk -- Increment the program break.
 *
 * Get incr bytes more RAM (for use by the heap).  malloc() and
 * friends call this function behind the scenes.
 */
void *
_sbrk(int incr)
{
	static void *pbreak = NULL;	/* current program break */
	void *ret;

	if (pbreak == NULL) {
		pbreak = CONFIG_HEAP_START;
	}

	if ((CONFIG_HEAP_END - pbreak < incr) ||
	    (pbreak - CONFIG_HEAP_START < -incr)) {
		errno = ENOMEM;
		return (void *)-1;
	}

	ret = pbreak;
	pbreak += incr;
	return ret;
}

__weak__ int
_open(const char *path, int flags, ...)
{
	return 1;
}

__weak__ int
_close(int fd)
{
	return 0;
}

__weak__ int
_fstat(int fd, struct stat *st)
{
	st->st_mode = S_IFCHR;
	return 0;
}

__weak__ int _isatty(int fd)
{
	return 1;
}

__weak__ int
isatty(int fd)
{
	return 1;
}

__weak__ int
_lseek(int fd, off_t pos, int whence)
{
	return -1;
}

__weak__ unsigned char
getch(void)
{
	return 0;
}

__weak__ int 
_read(int fd, char *buf, size_t cnt)
{
	*buf = getch();

	return 1;
}

__weak__ void 
putch(unsigned char c)
{
        if (c == '\n')
                usart_putc(USART1, '\r');

        usart_putc(USART1, c);
}

__weak__ void 
cgets(char *s, int bufsize)
{
	char *p;
	int c;
	int i;

	for (i = 0; i < bufsize; i++) {
		*(s + i) = 0;
	}
#if 0
        memset(s, 0, bufsize);
#endif
	p = s;

	for (p = s; p < s + bufsize - 1;) {
		c = getch();
		switch (c) {
		case '\r':
		case '\n':
			putch('\r');
			putch('\n');
			*p = '\n';
			return;

		case '\b':
			if (p > s) {
				*p-- = 0;
				putch('\b');
				putch(' ');
				putch('\b');
			}
			break;

		default:
			putch(c);
			*p++ = c;
			break;
		}
	}
	return;
}

__weak__ int 
_write(int fd, const char *buf, size_t cnt)
{
	size_t i;

	for (i = 0; i < cnt; i++)
		putch(buf[i]);

	return cnt;
}

/* Override fgets() in newlib with a version that does line editing */
__weak__ char *
fgets(char *s, int bufsize, void *f)
{
	cgets(s, bufsize);
	return s;
}

__weak__ void 
_exit(int exitcode)
{
	while (1) 
                /* do nothing */;
}
