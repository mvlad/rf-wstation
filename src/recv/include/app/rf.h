#ifndef __RF_H
#define __RF_H
/*
 * protocol for sending data over RF using serial:
 *
 * sending 4 bytes for 1 byte of data
 *
 * [ready to send data][start of frame 0][start of frame 1][destination frame][data frame][checksum frame]
 *      0xf0                    0x8f            0xaa            0xa1              0xXX          0xYY
 *
 */

/* 
 * synch byte, sent transmitter but never received
 */
#define RF_READY_TO_SEND        0xf0

/*
 * the follow 3 bytes are the header 
 */
#define RF_START0               0x8f
#define RF_START1               0xaa
#define RF_DST                  0xa1

/*
 * how many bytes has the header frame
 */
#define RF_FRAME_HEADER_SIZE    3

/*
 * how many bytes has the CRC
 */
#define RF_FRAME_CRC_SIZE       1

/*
 * sending 1-byte of data
 */
#define RF_FRAME_DATA_SIZE      1

/*
 * sending 4-bytes of data
 */
#define RF_FRAME_DATA_SIZE_BYTE 4

/*
 * size of the frame when receiving one byte of data
 */
#define RF_FRAME_SIZE           \
        (RF_FRAME_HEADER_SIZE + RF_FRAME_DATA_SIZE + RF_FRAME_CRC_SIZE)
/*
 * size of the frame when receiving 4-bytes of data
 */
#define RF_FRAME_SIZE_BYTE      \
        (RF_FRAME_HEADER_SIZE + RF_FRAME_DATA_SIZE_BYTE + RF_FRAME_CRC_SIZE)

/*
 * receive one byte of data
 */
extern int
rf_recv_byte(struct usart_dev *dev, uint8_t *data, int wait);

/*
 * wrapper around rf_recv_byte()
 */
extern int 
rf_recv_all_bytes(struct usart_dev *dev, uint8_t *dst, uint8_t len);

/*
 * receive 4-bytes of data
 */
extern int
rf_recv_mbytes(struct usart_dev *dev, uint8_t *data, uint8_t len, int wait);

#endif
