#ifndef __TIMER_H
#define __TIMER_H

extern volatile uint32_t __micros;

extern void
timer1_init(uint8_t prescaler, uint16_t ticks);

extern void
timer1_enable(void);

extern void
timer1_disable(void);

extern void
timer0_init(uint8_t prescaler, uint8_t ticks);

extern void
timer0_enable(void);

extern void
timer0_disable(void);

extern void
timer2_init(uint8_t prescaler, uint8_t ticks);

extern void
timer2_enable(void);

extern void
timer2_disable(void);


extern uint32_t 
micros(void);

#define INSTALL_TIMER()                 \
        ISR(TIMER1_COMPA_vect)  {       \
                __micros++;             \
        }
                
#endif
