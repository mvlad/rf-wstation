#include <avr/io.h>

#include "sys/usart.h"
#include "sys/printfmt.h"
#include "sys/console.h"

#define CONSBUFSIZE 512

static struct {
        uint8_t buf[CONSBUFSIZE];
        uint32_t rpos;
        uint32_t wpos;
} cons;

static int
serial_getc(void)
{
        int c;

        /* drain serial */
        usart_recv_ch();

        /* grab the next character from the input buffer. */
        if (cons.rpos != cons.wpos) {
                c = cons.buf[cons.rpos++];

                if (cons.rpos == CONSBUFSIZE) {
                        cons.rpos = 0;
                }

                return c;
        }
        return 0;
}

static void
serial_putc(int c)
{
        usart_send_ch(c);
}

static int
getchar(void)
{
        int c;
        while ((c = serial_getc()) == 0)
                ;
        return c;
}

char *
readline(const char *prompt)
{
        int i, c, echo;
        static char buf[CONSBUFSIZE];

        if (prompt != NULL) { 
                do_printf("%s", prompt);
        }

        i = 0; 
        echo = 1;

        while (1) { 
                c = getchar();
                if (c < 0) {
                        if (c != -E_EOF) {
                                do_printf("read err: %e\n", c);
                        }
                        return NULL;
                } else if ((c == '\b' || c == '\x7f') && i > 0) {
                        if (echo) {
                                serial_putc('\b');
                        }
                        i--; 
                } else if (c >= ' ' && (i < CONSBUFSIZE - 1)) {
                        if (echo) {
                                serial_putc(c);
                        }
                        buf[i++] = c;
                } else if (c == '\n' || c == '\r') {
                        if (echo) {
                                serial_putc('\n');
                        }
                        buf[i] = 0;
                        return buf;
                }
        }
}
