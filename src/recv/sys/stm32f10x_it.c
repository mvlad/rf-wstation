#include "stm32f10x_it.h"

#include "app/main.h"
#include "app/stm32.h"
#include "app/rb.h"
#include "app/pin.h"

uint16_t capture = 0;

extern __IO uint16_t CCR1_Val;

extern struct usart_dev usart1_dev;
extern struct usart_dev usart2_dev;

extern uint32_t ticks;
extern uint32_t jiffies;

/**
 * @brief  This function handles SVCall exception.
 * @param  None
 * @retval None
 */
void
SVC_Handler(void)
{
}

/**
 * @brief  This function handles Debug Monitor exception.
 * @param  None
 * @retval None
 */
void
DebugMon_Handler(void)
{
}

/**
 * @brief  This function handles PendSV_Handler exception.
 * @param  None
 * @retval None
 */
void
PendSV_Handler(void)
{
}

/**
 * @brief  This function handles SysTick Handler.
 * @param  None
 * @retval None
 */
void
SysTick_Handler(void)
{
        ticks++;
}

/******************************************************************************/
/*            STM32F10x Peripherals Interrupt Handlers                        */
/******************************************************************************/

void
USART1_IRQHandler(void)
{
        if (USART_GetITStatus(USART1, USART_IT_RXNE) != RESET) {
#if defined(RB_SAFE_INSERT) && (RB_SAFE_INSERT == 1)
                /* Read one byte from the receive data register */
                rb_safe_insert((&usart1_dev)->rb, USART_ReceiveData(USART1));
#else
                rb_push_insert((&usart1_dev)->rb, USART_ReceiveData(USART1));
#endif
        }
}

/**
 * @brief  This function handles USARTz global interrupt request.
 * @param  None
 * @retval None
 */
void
USART2_IRQHandler(void)
{
        if (USART_GetITStatus(USART2, USART_IT_RXNE) != RESET) {
#if defined(RB_SAFE_INSERT) && (RB_SAFE_INSERT == 1)
                /* Read one byte from the receive data register */
                rb_safe_insert((&usart2_dev)->rb, USART_ReceiveData(USART2));
#else
                rb_push_insert((&usart2_dev)->rb, USART_ReceiveData(USART2));
#endif
        }
}

void
TIM2_IRQHandler(void)
{
        if (TIM_GetITStatus(TIM2, TIM_IT_CC1) != RESET) {

                /* run cb function */
                if (timer2_cb) {
                        timer2_cb();
                }

                /* TIM2 Output compare pending bit */
                TIM_ClearITPendingBit(TIM2, TIM_IT_CC1);

                /* 
                 * reload the timer with the same value so the interrupt event
                 * will happen with at same period of time 
                 */
                capture = TIM_GetCapture1(TIM2);
                TIM_SetCompare1(TIM2, capture + CCR1_Val);

        }
}

void
RTC_IRQHandler(void)
{
        if (RTC_GetITStatus(RTC_IT_SEC) != RESET) {

                /* Clear the RTC Second interrupt */
                RTC_ClearITPendingBit(RTC_IT_SEC);

                /* Wait until last write operation
                 * on RTC registers has finished */
                RTC_WaitForLastTask();

        }
}

