#include <avr/io.h>
#include <util/delay.h>

#include "sys/usart.h"
#include "app/rf.h"

static uint8_t 
compute_crc8(uint8_t *frames, uint8_t len)
{
        uint8_t crc = 0;
        uint8_t i, mix, inb;
        uint8_t *d = frames;

        while (len--) {
                inb = *d++;

                for (i = 8; i; i--) {
                        mix = (crc ^ inb);
                        mix = mix & 0x01;

                        crc >>= 1;
                        if (mix) {
                                crc ^= 0x8c;
                        }

                        inb >>= 1;
                }
        }
        return crc;
}

void
rf_send_frame(uint8_t data, uint8_t ready)
{
        uint8_t frame[RF_FRAME_SIZE] = { 
                RF_START0, RF_START1, RF_DST, data, 0x00 
        };

        /* store the crc */
        frame[RF_FRAME_SIZE - 1] = 
                compute_crc8(frame, RF_FRAME_SIZE - 1);


        /* ready to send, junk byte for synchronization */
        if (ready) {
                usart_send_byte(RF_READY_TO_SEND);
                usart_send_byte(RF_READY_TO_SEND);
        }

        /* frame header */
        usart_send_byte(frame[0]);
        usart_send_byte(frame[1]);
        usart_send_byte(frame[2]);

        /* data */
        usart_send_byte(frame[3]);

        /* checksum */
        usart_send_byte(frame[RF_FRAME_SIZE - 1]);
}

void
rf_send_frame_bytes(uint8_t *data, uint8_t len, uint8_t ready)
{
        uint8_t i, j;

        uint8_t frame_header[RF_FRAME_HEADER_SIZE] = { 
                RF_START0, RF_START1, RF_DST
        };

        /* required to compute the crc */
        uint8_t frame[RF_FRAME_SIZE_BYTE];

        frame[0] = frame_header[0]; /* start 0 */
        frame[1] = frame_header[1]; /* start 1 */
        frame[2] = frame_header[2]; /* dst */

        /* add the data */
        for (i = 3, j = 0; j < len; i++, j++) {
                frame[i] = data[j];
        }

        /* add the crc */
        frame[RF_FRAME_SIZE_BYTE - 1] = 
                compute_crc8(frame, RF_FRAME_SIZE_BYTE - 1);


        /* ready to send, junk byte for synchronization */
        if (ready) {
                usart_send_byte(RF_READY_TO_SEND);
        }

        /* frame header */
        usart_send_byte(frame[0]);
        usart_send_byte(frame[1]);
        usart_send_byte(frame[2]);

        /* data */
        for (i = 0; i < len; i++)
                usart_send_byte(data[i]);

        /* and finally, send the checksum */
        usart_send_byte(frame[RF_FRAME_SIZE_BYTE - 1]);
}

void
rf_send(uint8_t *d, uint8_t len)
{
        uint8_t i;

        for (i = 0; i < len; i++) {
                rf_send_frame(*d++, 1);
        }
}
