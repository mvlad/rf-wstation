#include <avr/io.h>
#include "sys/i2c.h"

/*
 * SCK freq = F_OSC / (16 + 2 * TWBR * Prescaler)
 */

void
i2c_write(uint8_t byte)
{
        TWDR = byte;

        TWCR = (1 << TWINT) | (1 << TWEN);

        /* wait until write */
        while ((TWCR & (1 << TWINT)) == 0)
                ;

}

uint8_t
i2c_read(void)
{
        TWCR = (1 << TWINT) | (1 << TWEN);

        /* wait until data is read */
        while ((TWCR & (1 << TWINT)) == 0)
                ;

        return TWDR;
}

void
i2c_start(void)
{
        /* send start */
        TWCR = (1 << TWINT) | (1 << TWSTA) | (1 << TWEN);

        /* wait until START has been transmited */
        while ((TWCR & (1 << TWINT)) == 0)
                ;
}

void
i2c_stop(void)
{
        /* send stop */
        TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWSTO);
}

void
i2c_init(void)
{
        /* prescaler bits to 0 */
        TWSR = 0x00;

        /* SCL freq 50KHz for 8MHz F_OSC */
        TWBR = 0x47;

        /* enable TWI */
        TWCR |= _BV(TWEN);
}

void
i2c_disable(void)
{
        /* disable TWI */
        TWCR |= ~_BV(TWEN);
}
