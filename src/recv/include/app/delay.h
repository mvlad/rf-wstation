#ifndef __DELAY_H
#define __DELAY_H

/*
 * a spin-loop used to implement delay
 */
extern void
_delay_ms(uint32_t ms);

#endif 
