#include "sys/stm32f10x_conf.h"

#include "app/lcd-usart.h"
#include "app/delay.h"
#include "app/rb.h"
#include "app/usart.h"


static void
lcd_special_command(uint8_t byte)
{
        usart_putc(USART1, COMMAND_LCD_INIT);
        usart_putc(USART1, byte);
        _delay_ms(5);
}

static void
lcd_command(uint8_t byte)
{
        usart_putc(USART1, COMMAND_LCD_CONTROL);
        usart_putc(USART1, byte);
        _delay_ms(5);
}

void 
lcd_init(void)
{
        /* set brightness */
        lcd_special_command(PWM_OFF);
        _delay_ms(100);

        lcd_special_command(PWM_FULL);
        _delay_ms(100);

        lcd_special_command(0x06);
        lcd_special_command(0x04);

        lcd_clear();
        lcd_return_home();
}

void
lcd_clear(void)
{
        lcd_command(CLEAR_LCD);
}

void
lcd_return_home(void)
{
        lcd_command(RETURN_HOME);
}

/*
 * On the screen we have:
 *  - -            --
 * |0|1| ...      |15|
 * +-----------------+
 * |0|1| ...      |15|
 *  - -            --
 *
 * But we need to select from 1:
 *
 * row 0 will be row 1
 * col 1 will be col 1
 *
 * Example: to write at col 0, row 14, we would say 
 * lcd_set_cursor(1, 15)
 */
void
lcd_set_cursor(uint8_t row, uint8_t col)
{
        int row_off[4] = { 0x00, 0x40, 0x10, 0x50 };

        lcd_command(0x80 | ((col - 1) + row_off[row - 1]));
}

void
lcd_left_to_right(void)
{
        lcd_command(0x04 + MOVE_CURSOR_LEFT);
}
