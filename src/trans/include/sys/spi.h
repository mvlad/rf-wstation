#ifndef __SPI_H
#define __SPI_H

#define SS      PINB2
#define MOSI    PINB3
#define MISO    PINB4
#define SCK     PINB5

extern void
spi_init(void);

extern void
spi_disable(void);

extern void
spi_send_command(uint8_t c, uint8_t d);

extern void
spi_send_simple(uint8_t c);


#endif /* __SPI_H */
