#ifndef __CONSOLE_H
#define __CONSOLE_H

#ifndef NULL
#define NULL (0)
#endif

#ifndef E_EOF
#define E_EOF (-1)
#endif

extern char *
readline(const char *prmt);

#endif
