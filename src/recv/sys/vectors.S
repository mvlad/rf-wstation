.thumb

/* Default handler for all non-overridden interrupts and exceptions */
	.globl  Default_Handler	
	.type	Default_Handler, %function

Default_Handler:
	b .

        .weak	NMI_Handler
        .globl	NMI_Handler
        .set    NMI_Handler, Default_Handler

        .weak	HardFault_Handler
        .globl	HardFault_Handler
        .set    HardFault_Handler, Default_Handler

        .weak	MemManage_Handler
        .globl	MemManage_Handler
        .set    MemManage_Handler, Default_Handler

        .weak	BusFault_Handler
        .globl	BusFault_Handler
        .set    BusFault_Handler, Default_Handler

        .weak	UsageFault_Handler
        .globl	UsageFault_Handler
        .set    UsageFault_Handler, Default_Handler

	.weak	__stm32reservedexception7
	.globl	__stm32reservedexception7
	.set	__stm32reservedexception7, Default_Handler

	.weak	__stm32reservedexception8
	.globl	__stm32reservedexception8
	.set	__stm32reservedexception8, Default_Handler

	.weak	__stm32reservedexception9
	.globl	__stm32reservedexception9
	.set	__stm32reservedexception9, Default_Handler 

	.weak	__stm32reservedexception10
	.globl	__stm32reservedexception10
	.set	__stm32reservedexception10, Default_Handler

        .weak	SVC_Handler
        .globl	SVC_Handler
        .set    SVC_Handler, Default_Handler

        .weak	DebugMon_Handler
        .globl	DebugMon_Handler
        .set    DebugMon_Handler, Default_Handler

	.weak	__stm32reservedexception13
	.globl	__stm32reservedexception13
	.set	__stm32reservedexception13, Default_Handler

        .weak	PendSV_Handler
        .globl	PendSV_Handler
        .set    PendSV_Handler, Default_Handler

        .weak	SysTick_Handler
        .globl	SysTick_Handler
        .set    SysTick_Handler, Default_Handler

        .weak	WWDG_IRQHandler
        .globl	WWDG_IRQHandler
        .set    WWDG_IRQHandler, Default_Handler

        .weak	PVD_IRQHandler
        .globl	PVD_IRQHandler
        .set    PVD_IRQHandler, Default_Handler

        .weak	TAMPER_IRQHandler
        .globl	TAMPER_IRQHandler
        .set    TAMPER_IRQHandler, Default_Handler

        .weak	RTC_IRQHandler
        .globl	RTC_IRQHandler
        .set    RTC_IRQHandler, Default_Handler

        .weak	FLASH_IRQHandler
        .globl	FLASH_IRQHandler
        .set    FLASH_IRQHandler, Default_Handler

        .weak	RCC_IRQHandler
        .globl	RCC_IRQHandler
        .set    RCC_IRQHandler, Default_Handler

        .weak	EXTI0_IRQHandler
        .globl	EXTI0_IRQHandler
        .set    EXTI0_IRQHandler, Default_Handler

        .weak	EXTI1_IRQHandler
        .globl	EXTI1_IRQHandler
        .set    EXTI1_IRQHandler, Default_Handler

        .weak	EXTI2_IRQHandler
        .globl	EXTI2_IRQHandler
        .set    EXTI2_IRQHandler, Default_Handler

        .weak	EXTI3_IRQHandler
        .globl	EXTI3_IRQHandler
        .set    EXTI3_IRQHandler, Default_Handler

        .weak	EXTI4_IRQHandler
        .globl	EXTI4_IRQHandler
        .set    EXTI4_IRQHandler, Default_Handler

        .weak	DMA1_Channel1_IRQHandler
        .globl	DMA1_Channel1_IRQHandler
        .set    DMA1_Channel1_IRQHandler, Default_Handler

        .weak	DMA1_Channel2_IRQHandler
        .globl	DMA1_Channel2_IRQHandler
        .set    DMA1_Channel2_IRQHandler, Default_Handler

        .weak	DMA1_Channel3_IRQHandler
        .globl	DMA1_Channel3_IRQHandler
        .set    DMA1_Channel3_IRQHandler, Default_Handler

        .weak	DMA1_Channel4_IRQHandler
        .globl	DMA1_Channel4_IRQHandler
        .set    DMA1_Channel4_IRQHandler, Default_Handler

        .weak	DMA1_Channel5_IRQHandler
        .globl	DMA1_Channel5_IRQHandler
        .set    DMA1_Channel5_IRQHandler, Default_Handler

        .weak	DMA1_Channel6_IRQHandler
        .globl	DMA1_Channel6_IRQHandler
        .set    DMA1_Channel6_IRQHandler, Default_Handler

        .weak	DMA1_Channel7_IRQHandler
        .globl	DMA1_Channel7_IRQHandler
        .set    DMA1_Channel7_IRQHandler, Default_Handler

        .weak	ADC1_2_IRQHandler
        .globl	ADC1_2_IRQHandler
        .set    ADC1_2_IRQHandler, Default_Handler

        .weak	USB_HP_CAN1_TX_IRQHandler
        .globl	USB_HP_CAN1_TX_IRQHandler
        .set    USB_HP_CAN1_TX_IRQHandler, Default_Handler

        .weak	USB_LP_CAN1_RX0_IRQHandler
        .globl	USB_LP_CAN1_RX0_IRQHandler
        .set    USB_LP_CAN1_RX0_IRQHandler, Default_Handler

        .weak	CAN1_RX1_IRQHandler
        .globl	CAN1_RX1_IRQHandler
        .set    CAN1_RX1_IRQHandler, Default_Handler

        .weak	CAN1_SCE_IRQHandler
        .globl	CAN1_SCE_IRQHandler
        .set    CAN1_SCE_IRQHandler, Default_Handler

        .weak	EXTI9_5_IRQHandler
        .globl	EXTI9_5_IRQHandler
        .set    EXTI9_5_IRQHandler, Default_Handler

        .weak	TIM1_BRK_IRQHandler
        .globl	TIM1_BRK_IRQHandler
        .set    TIM1_BRK_IRQHandler, Default_Handler

        .weak	TIM1_UP_IRQHandler
        .globl	TIM1_UP_IRQHandler
        .set    TIM1_UP_IRQHandler, Default_Handler

        .weak	TIM1_TRG_COM_IRQHandler
        .globl	TIM1_TRG_COM_IRQHandler
        .set    TIM1_TRG_COM_IRQHandler, Default_Handler

        .weak	TIM1_CC_IRQHandler
        .globl	TIM1_CC_IRQHandler
        .set    TIM1_CC_IRQHandler, Default_Handler

        .weak	TIM2_IRQHandler
        .globl	TIM2_IRQHandler
        .set    TIM2_IRQHandler, Default_Handler

        .weak	TIM3_IRQHandler
        .globl	TIM3_IRQHandler
        .set    TIM3_IRQHandler, Default_Handler

        .weak	TIM4_IRQHandler
        .globl	TIM4_IRQHandler
        .set    TIM4_IRQHandler, Default_Handler

        .weak	I2C1_EV_IRQHandler
        .globl	I2C1_EV_IRQHandler
        .set    I2C1_EV_IRQHandler, Default_Handler

        .weak	I2C1_ER_IRQHandler
        .globl	I2C1_ER_IRQHandler
        .set    I2C1_ER_IRQHandler, Default_Handler

        .weak	I2C2_EV_IRQHandler
        .globl	I2C2_EV_IRQHandler
        .set    I2C2_EV_IRQHandler, Default_Handler

        .weak	I2C2_ER_IRQHandler
        .globl	I2C2_ER_IRQHandler
        .set    I2C2_ER_IRQHandler, Default_Handler

        .weak	SPI1_IRQHandler
        .globl	SPI1_IRQHandler
        .set    SPI1_IRQHandler, Default_Handler

        .weak	SPI2_IRQHandler
        .globl	SPI2_IRQHandler
        .set    SPI2_IRQHandler, Default_Handler

        .weak	USART1_IRQHandler
        .globl	USART1_IRQHandler
        .set    USART1_IRQHandler, Default_Handler

        .weak	USART2_IRQHandler
        .globl	USART2_IRQHandler
        .set    USART2_IRQHandler, Default_Handler

        .weak	USART3_IRQHandler
        .globl	USART3_IRQHandler
        .set    USART3_IRQHandler, Default_Handler

        .weak	EXTI15_10_IRQHandler
        .globl	EXTI15_10_IRQHandler
        .set    EXTI15_10_IRQHandler, Default_Handler

        .weak	RTCAlarm_IRQHandler
        .globl	RTCAlarm_IRQHandler
        .set    RTCAlarm_IRQHandler, Default_Handler

        .weak	USBWakeUp_IRQHandler
        .globl	USBWakeUp_IRQHandler
        .set    USBWakeUp_IRQHandler, Default_Handler

/* STM32F1 performance line vector table */
.section ".stm32.interrupt_vector"

.globl	__stm32_vector_table
.type	__stm32_vector_table, %object

__stm32_vector_table:
/* CM3 core interrupts */
	.long	__msp_init
	.long	Reset_Handler
	.long	NMI_Handler
	.long	HardFault_Handler
	.long	MemManage_Handler
	.long	BusFault_Handler
	.long	UsageFault_Handler
	.long	__stm32reservedexception7
	.long	__stm32reservedexception8
	.long	__stm32reservedexception9
	.long	__stm32reservedexception10
	.long	SVC_Handler
	.long	DebugMon_Handler
	.long	__stm32reservedexception13
	.long	PendSV_Handler
	.long	SysTick_Handler
/* Peripheral interrupts */
	.long	WWDG_IRQHandler
	.long	PVD_IRQHandler
	.long	TAMPER_IRQHandler
	.long	RTC_IRQHandler
	.long	FLASH_IRQHandler
	.long	RCC_IRQHandler
	.long	EXTI0_IRQHandler
	.long	EXTI1_IRQHandler
	.long	EXTI2_IRQHandler
	.long	EXTI3_IRQHandler
	.long	EXTI4_IRQHandler
	.long	DMA1_Channel1_IRQHandler
	.long	DMA1_Channel2_IRQHandler
	.long	DMA1_Channel3_IRQHandler
	.long	DMA1_Channel4_IRQHandler
	.long	DMA1_Channel5_IRQHandler
	.long	DMA1_Channel6_IRQHandler
	.long	DMA1_Channel7_IRQHandler
	.long	ADC1_2_IRQHandler
	.long	USB_HP_CAN1_TX_IRQHandler
	.long	USB_LP_CAN1_RX0_IRQHandler
	.long	CAN1_RX1_IRQHandler
	.long	CAN1_SCE_IRQHandler
	.long	EXTI9_5_IRQHandler
	.long	TIM1_BRK_IRQHandler
	.long	TIM1_UP_IRQHandler
	.long	TIM1_TRG_COM_IRQHandler
	.long	TIM1_CC_IRQHandler
	.long	TIM2_IRQHandler
	.long	TIM3_IRQHandler
	.long	TIM4_IRQHandler
	.long	I2C1_EV_IRQHandler
	.long	I2C1_ER_IRQHandler
	.long	I2C2_EV_IRQHandler
	.long	I2C2_ER_IRQHandler
	.long	SPI1_IRQHandler
	.long	SPI2_IRQHandler
	.long	USART1_IRQHandler
	.long	USART2_IRQHandler
	.long	USART3_IRQHandler
	.long	EXTI15_10_IRQHandler
	.long	RTCAlarm_IRQHandler
	.long	USBWakeUp_IRQHandler

	.size	__stm32_vector_table, . - __stm32_vector_table
