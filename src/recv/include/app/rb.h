#ifndef __RING_BUFF_H
#define __RING_BUFF_H

/*
 * the size of the buffer
 */
#define RX_BUF  64
#define RB_SAFE_INSERT  0

/*
 * storage for keeping track of items in the buffer
 *
 * The actual size of the buffer will be size - 1.
 */
struct rb {
        volatile uint8_t *buf;

        uint8_t head;
        uint8_t tail;
        uint8_t size;
};

/*
 * a useful structure for accessing the ring buffer and usart devices
 */
struct usart_dev {
        USART_TypeDef *usart;
        struct rb *rb;
        uint8_t raw[RX_BUF];
};

/*
 * count the items in the buffer
 */
extern uint16_t
rb_count(struct rb *rb);

/*
 * determine if the buffer is full
 */
extern int
rb_is_full(struct rb *rb);

/*
 * determine if the buffer is empty
 */
extern int
rb_is_empty(struct rb *rb);

/*
 * remove an item from the buffer
 */
extern uint8_t
rb_remove(struct rb *rb);

/*
 * remove an item from the buffer is the buffer is not full
 */
extern int16_t
rb_safe_remove(struct rb *rb);

/*
 * adds an item in the buffer, only if there is room
 */
extern int
rb_safe_insert(struct rb *rb, uint8_t v);

/*
 * adds an item in the buffer, by removing the last item
 */
extern int
rb_push_insert(struct rb *rb, uint8_t v);

/*
 * make the tail and head of the ring buffer 0
 */
extern void
rb_reset(struct rb *rb);

/*
 * connect a usart device to a real usart interface and ring buffer
 */
extern void
dev_init(struct usart_dev *d, USART_TypeDef *usart, struct rb *rb);

#endif
