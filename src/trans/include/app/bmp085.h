#ifndef __BMP_085_H
#define __BMP_085_H

/* BMP modes of operation */
#define BMP_ULTRALOWPOWER    0
#define BMP_STANDARD         1
#define BMP_HIGHRES          2
#define BMP_ULTRAHIGHRES     3

/* BMP calibration registers */
#define BMP_CAL_AC1          0xaa
#define BMP_CAL_AC2          0xac
#define BMP_CAL_AC3          0xae
#define BMP_CAL_AC4          0xb0
#define BMP_CAL_AC5          0xb2
#define BMP_CAL_AC6          0xb4

#define BMP_CAL_B1           0xb6
#define BMP_CAL_B2           0xb8

#define BMP_CAL_MB           0xba
#define BMP_CAL_MC           0xbc
#define BMP_CAL_MD           0xbe

/* BMP commands */
/* address of the I2C slave */
#define BMP_I2CADDR          0x77

#define BMP_READY_TO_READ    0xef       /* 0x77 + 1bit (0) for R */
#define BMP_READY_TO_WRITE   0xee       /* 0x77 + 1bit (1) for W */

#define BMP_CONTROL          0xF4 

#define BMP_TEMPDATA         0xF6
#define BMP_PRESSUREDATA     0xF6

#define BMP_READTEMPCMD      0x2E
#define BMP_READPRESSURECMD  0x34

struct bmp_data {
        int32_t temp;
        int32_t pressure;
};

extern void
bmp_init(int mode);

extern int32_t
bmp_get_temp(void);

extern int32_t
bmp_get_pressure(void);

void
bmp_get_all(volatile struct bmp_data *data);

#endif /* __BMP_085_H */
