#include <avr/io.h>
#include <avr/interrupt.h>
#include "sys/eeprom.h"

void
eeprom_write(uint16_t addr, uint8_t b)
{
        cli();

        /* wait until previous write */
        while (EECR & (1 << EEPE))
                ;

        EEAR = addr;
        EEDR = b;

        EECR |= _BV(EEMPE) | _BV(EEPE);

        sei();
}

uint8_t
eeprom_read(uint16_t addr)
{
        uint8_t data;
        cli();

        /* wait until previous write */
        while (EECR & (1 << EEPE))
                ;

        EEAR = addr;

        EECR |= _BV(EERE);

        data = EEDR;
        sei();
        return data;
}

void
write_no_to_eeprom(uint32_t d)
{
        eeprom_write(1000, (d >> 24) & 0xff);
        eeprom_write(1001, (d >> 16) & 0xff);
        eeprom_write(1002, (d >> 8) & 0xff);
        eeprom_write(1003, d & 0xff);
}

uint32_t
read_no_from_eeprom(void)
{
        uint32_t d = 0;
        uint16_t i = 1003;

        while (i-- > 1000) {
                d |= eeprom_read(i);
                d >>= 8;
        }
        return d;
}
