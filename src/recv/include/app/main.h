#ifndef __MAIN_H
#define __MAIN_H

/*
 * clock info
 */
struct clock {
        uint8_t secs;
        uint8_t mins;
        uint8_t hours;
};

/*
 * data from sensor
 */
struct bmp {
        int32_t temp;
        int32_t pressure;
};

void (*timer2_cb)(void);


#endif  /* __MAIN_H */
