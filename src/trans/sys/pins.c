#include <avr/io.h>
#include "sys/pins.h"

void
pins_clear(void)
{
        PIN_OFF(D, 5);
        PIN_OFF(D, 6);
}

void
pins_init(void)
{
        /* this pin will pulse in main loop  */
        PIN_OUTPUT(D, 5);

        /* this pin will pulse in the timer interrupt */
        PIN_OUTPUT(D, 6);

        /* D2 is hooked to a button, and trigged in the interrupt routine */
        PIN_INPUT(D, 2);

        /* change the hrs */
        PIN_INPUT(D, 3);

        /* change the mins */
        PIN_INPUT(D, 4);
}
