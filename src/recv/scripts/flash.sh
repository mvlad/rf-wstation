#!/bin/sh

[ -z "$1" ] && echo 1

[ -z "$2" ] && echo 1

case "$1" in
        flash)
        echo "Burning to flash..."
        openocd -f scripts/openocd.cfg \
                -c 'reset halt' \
                -c 'flash probe 0' \
                -c 'flash write_image erase '$2' 0x08000000'\
                -c 'reset' -c 'exit' 2>/dev/null
        ;;
        ram)
        echo "Burning to RAM..."
        openocd -f scripts/openocd.cfg \
                -c 'reset halt' \
                -c 'flash probe 0' \
                -c 'flash write_image erase '$2' 0x20000000'\
                -c 'reset' -c 'exit' 2>/dev/null
        ;;
        *)
                exit 1
        ;;
esac

echo "Done"
exit 0
