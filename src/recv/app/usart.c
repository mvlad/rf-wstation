#include "sys/stm32f10x_conf.h"

#include "app/rb.h"
#include "app/usart.h"

void
usart_putc(USART_TypeDef *dev, uint8_t ch)
{
        USART_SendData(dev, ch);

        /* wait data is flushed */
        while (USART_GetFlagStatus(dev, USART_FLAG_TXE) == RESET)
                ;
}

void
usart_putstr(USART_TypeDef *dev, const char *str)
{
        const char *p = str;
        while (p && *p) {
                uint8_t ch = *p++;
                usart_putc(dev, ch); 
        }
}

uint32_t
usart_data_available(struct usart_dev *d)
{
        return rb_count(d->rb);
}

uint8_t
usart_getc(struct usart_dev *d)
{
        return rb_remove(d->rb);
}

void
usart_reset(struct usart_dev *d)
{
        rb_reset(d->rb);
}
