#include "sys/stm32f10x_conf.h"

#include "app/stm32.h"
#include "app/delay.h"

__inline void 
_delay_us(uint32_t us)
{
        us *= STM32_DELAY_US_MULT;

        /* fudge for function call overhead  */
        us--;
        __asm volatile("mov r0, %[us]   \n\t"
                       "1: subs r0, #1  \n\t"
                        "bhi 1b         \n\t" 
                        : : [us] "r" (us) : "r0");
}

void
_delay_ms(uint32_t ms)
{
        uint32_t i;

        for (i = 0; i < ms; i++) {
                _delay_us(1000);
        }
}
