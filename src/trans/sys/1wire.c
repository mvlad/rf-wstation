#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <errno.h>

#include "sys/1wire.h"

void
owm_init(void)
{
        RELEASE_BUS();

        _delay_us(DELAY_H_STD_MODE);    /* 480 */
}

uint8_t 
owm_detect_presence(void)
{
        uint8_t presence_detected;

        cli();

        /* set pin 2 on PORTB2 as output */
        PUT_BUS_LOW();
        _delay_us(DELAY_H_STD_MODE);    /* 480 */

        RELEASE_BUS();
        _delay_us(DELAY_I_STD_MODE);    /* 70 */

        presence_detected = OW_BIT;
        _delay_us(DELAY_J_STD_MODE);    /* 410 */

        sei();

        return presence_detected;
}


uint8_t
owm_read_bit(void)
{
        uint8_t r;

        cli();
        /* set pin 2 on PORB2 as output */
        PUT_BUS_LOW();
        _delay_us(DELAY_A_STD_MODE);    /* 6 */

        RELEASE_BUS();
        _delay_us(DELAY_E_STD_MODE);    /* 9 */

        /* read it */
        r = OW_BIT;
        _delay_us(DELAY_F_STD_MODE);    /* 55 */
        sei();

        return r;
}

void
owm_write_bit(uint8_t b)
{
        if (b & 1) {
                cli();
                /* set pin 2 on PORTB2 as output */
                PUT_BUS_LOW();
                _delay_us(DELAY_A_STD_MODE);    /* 6 */

                RELEASE_BUS();
                _delay_us(DELAY_B_STD_MODE);    /* 64 */
                sei();
        } else {
                cli();
                PUT_BUS_LOW();
                _delay_us(DELAY_C_STD_MODE);    /* 60 */

                RELEASE_BUS();
                _delay_us(DELAY_D_STD_MODE);    /* 10 */
                sei();
        }
}

uint8_t
owm_read_byte(void)
{
        uint8_t r = 0x00;
        uint8_t mask;

        for (mask = 0x01; mask; mask <<= 1) {
                if (ows_read_bit())
                        r |= mask;
        }
        return r;
}


void
owm_write_byte(uint8_t byte)
{
        uint8_t mask;
        for (mask = 0x01; mask; mask <<= 1) {
                if (mask & byte)
                        owm_write_bit(1);
                else
                        owm_write_bit(0);
        }
}

uint8_t
ows_detect_reset(void)
{
        return 1;
}

uint8_t
ows_send_presence(void)
{
        uint8_t r;

        cli();
        PUT_BUS_LOW();
        sei();

        _delay_us(120);

        cli();

        RELEASE_BUS();

        _delay_us(300 - 25);
        r = OW_BIT;
        sei();

        if (!r) {
                return 1;
        }

        return 0;
}



static uint8_t 
wait_timeout(uint8_t rw)
{
        uint16_t retries = RETRY_MAX_COUNT;

        while (!OW_BIT) {
                if (--retries == 0) {
                        return 20;
                }
        }

        if (rw)
                retries = RETRY_MAX_COUNT;
        else
                retries = RETRY_MAX_COUNT_READ;

        while (OW_BIT) {
                if (--retries == 0) {
                        return 20;
                }
        }

        return 1;
}

uint8_t
ows_read_bit(void)
{
        uint8_t r = 0;
        uint8_t wait;

        cli();

        RELEASE_BUS();

        wait = wait_timeout(0);
        if (wait != 1) {
                if (wait == 10) {
                        errno = ERR_TIMEOUT;
                } else if (wait == 20) {
                        errno = ERR_TIMEOUT_READ;
                }
                sei();
                return 0;
        }

        _delay_us(30);
        r = OW_BIT;

        sei();

        return r;
}

void 
ows_write_bit(uint8_t bit)
{
        uint8_t wait;

        cli();
        RELEASE_BUS();

        wait = wait_timeout(1);

        if (wait != 1) {
                if (wait == 10) {
                        errno = ERR_TIMEOUT_WRITE;
                } else if (wait == 20) {
                        errno = ERR_TIMEOUT_WRITE;
                }
                sei();
                return;
        }

        if (bit & 1) {
                _delay_us(30);
        } else {

                PUT_BUS_LOW();
                _delay_us(30);

                PUT_BUS_HIGH();
        }

        sei();
}

uint8_t
ows_read_byte(void)
{
        uint8_t r = 0x00;
        uint8_t mask;

        for (mask = 0x01; mask; mask <<= 1) {
                if (ows_read_bit())
                        r |= mask;
        }
        return r;
}

void
ows_write_byte(uint8_t byte)
{
        uint8_t mask;
        for (mask = 0x01; mask; mask <<= 1) {
                if (mask & byte)
                        ows_write_bit(1);
                else
                        ows_write_bit(0);
        }
}
