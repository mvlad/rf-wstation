#ifndef __EEPROM_H
#define __EEPROM_H

extern void
eeprom_write(uint16_t addr, uint8_t b);

extern uint8_t
eeprom_read(uint16_t addr);

extern uint32_t
read_no_from_eeprom(void);

extern void
write_no_to_eeprom(uint32_t d);

#endif
