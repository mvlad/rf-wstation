#ifndef __USART_H
#define __USART_H

/*
 * send a byte over usart
 */
extern void
usart_putc(USART_TypeDef *dev, uint8_t ch);

/*
 * remove a byte from ring buffer
 */
extern uint8_t
usart_getc(struct usart_dev *d);

/*
 * send/put a string over usart
 */
extern void
usart_putstr(USART_TypeDef *dev, const char *str);

/*
 * count how many items are in the ring buffer
 */
extern uint32_t
usart_data_available(struct usart_dev *d);

#endif
