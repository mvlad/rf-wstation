#include <avr/io.h>
#include <avr/interrupt.h>

#include "sys/timer.h"

/*
 * Timer1
 * Choosing prescaler and timer values:
 *
 * Scaler 
 *
 * None -> 1
 * 8    -> 2
 * 64   -> 3
 * 256  -> 4
 * 1024 -> 5
 *
 * Timer values for 3 second period
 *
 * Scaler       Timer Clock             Timer period            Timer Value
 * None         8MHz                    1/8MHz = 0.125us        3s / 0.125us = 24M
 * 8            8MHz/8 = 1MHz           1/1MHz = 1us            3s / 1us = 3M
 * 64           8MHz/64 = 125kHz        1/125kHz = 8us          3s / 8us = 375000
 * 256          8MHz/256 = 31.25kHz     1/31.25kHz = 32us       3s / 32us = 93750
 * 1024         8MHz/1024 = 7.8125kHz   1/7.8125kHz = 128us     3s / 128us = 23437.5
 *
 * Timer values for 1 second period
 *
 * Scaler       Timer Clock             Timer period            Timer Value
 * None         8MHz                    1/8MHz = 0.125us        1s / 0.125us = 8M
 * 8            8MHz/8 = 1MHz           1/1MHz = 1us            1s / 1us = 1M
 * 64           8MHz/64 = 125kHz        1/125kHz = 8us          1s / 8us = 125000
 * 256          8MHz/256 = 31.25kHz     1/31.25kHz = 32us       1s / 32us = 31250
 * 1024         8MHz/1024 = 7.8125kHz   1/7.8125kHz = 128us     1s / 128us = 7812.5
 *
 * Timer values for 0.5 second period
 *
 * Scaler       Timer Clock             Timer period            Timer Value
 * None         8MHz                    1/8MHz = 0.125us        0.5s / 0.125us = 4M
 * 8            8MHz/8 = 1MHz           1/1MHz = 1us            0.5s / 1us = 500000
 * 64           8MHz/64 = 125kHz        1/125kHz = 8us          0.5s / 8us = 62500
 * 256          8MHz/256 = 31.25kHz     1/31.25kHz = 32us       0.5s / 32us = 15625
 * 1024         8MHz/1024 = 7.8125kHz   1/7.8125kHz = 128us     0.5s / 128us = 3906.25
 *
 * Timer values for 0.25 second period (250ms)
 *
 * Scaler       Timer Clock             Timer period            Timer Value
 * None         8MHz                    1/8MHz = 0.125us        0.25s / 0.125us = 2M
 * 8            8MHz/8 = 1MHz           1/1MHz = 1us            0.25s / 1us = 250000
 * 64           8MHz/64 = 125kHz        1/125kHz = 8us          0.25s / 8us = 31250
 * 256          8MHz/256 = 31.25kHz     1/31.25kHz = 32us       0.25s / 32us = 7812.5
 * 1024         8MHz/1024 = 7.8125kHz   1/7.8125kHz = 128us     0.25s / 128us = 1953.12
 *
 * Timer values for 0.1 second period (100ms)
 *
 * Scaler       Timer Clock             Timer period            Timer Value
 * None         8MHz                    1/8MHz = 0.125us        0.1s / 0.125us = 800000
 * 8            8MHz/8 = 1MHz           1/1MHz = 1us            0.1s / 1us = 100000
 * 64           8MHz/64 = 125kHz        1/125kHz = 8us          0.1s / 8us = 12500
 * 256          8MHz/256 = 31.25kHz     1/31.25kHz = 32us       0.1s / 32us = 3125
 * 1024         8MHz/1024 = 7.8125kHz   1/7.8125kHz = 128us     0.1s / 128us = 781.25
 *
 * Timer values for 0.01 second period (10ms)
 *
 * Scaler       Timer Clock             Timer period            Timer Value
 * None         8MHz                    1/8MHz = 0.125us        0.01s / 0.125us = 80000
 * 8            8MHz/8 = 1MHz           1/1MHz = 1us            0.01s / 1us = 10000
 * 64           8MHz/64 = 125kHz        1/125kHz = 8us          0.01s / 8us = 1250
 * 256          8MHz/256 = 31.25kHz     1/31.25kHz = 32us       0.01s / 32us = 312.5
 * 1024         8MHz/1024 = 7.8125kHz   1/7.8125kHz = 128us     0.01s / 128us = 78.125
 *
 * Timer values for 0.001 second period (1ms)
 *
 * Scaler       Timer Clock             Timer period            Timer Value
 * None         8MHz                    1/8MHz = 0.125us        0.001s / 0.125us = 8000
 * 8            8MHz/8 = 1MHz           1/1MHz = 1us            0.001s / 1us = 1000
 * 64           8MHz/64 = 125kHz        1/125kHz = 8us          0.001s / 8us = 125
 * 256          8MHz/256 = 31.25kHz     1/31.25kHz = 32us       0.001s / 32us = 31.25
 * 1024         8MHz/1024 = 7.8125kHz   1/7.8125kHz = 128us     0.001s / 128us = 7.8125
 *
 *
 * Timer0
 *
 * prescaler values
 * None -> 1
 * 8    -> 2
 * 64   -> 3
 * 256  -> 4
 * 1024 -> 5
 *
 * 1ms
 *
 * Scaler       Timer Clock             Timer period            Timer Value
 * None         8MHz                    1/8MHz = 0.125us        0.001s / 0.125us = 8000
 * 8            8MHz/8 = 1MHz           1/1MHz = 1us            0.001s / 1us = 1000
 * 64           8MHz/64 = 125kHz        1/125kHz = 8us          0.001s / 8us = 125
 * 256          8MHz/256 = 31.25kHz     1/31.25kHz = 32us       0.001s / 32us = 31.25
 * 1024         8MHz/1024 = 7.8125kHz   1/7.8125kHz = 128us     0.001s / 128us = 7.8125
 *
 */

volatile uint32_t __micros = 0;

void
timer1_init(uint8_t prescaler, uint16_t ticks)
{
        /* Output Compare pins disconnected */
        TCCR1A = 0;

        /* Turn on CTC mode */
        TCCR1B = _BV(WGM12);

        /* convert prescaler index to TCCRnB prescaler bits CS10, CS11, CS12 */
        TCCR1B |= prescaler;

        OCR1A = ticks;
}

void
timer1_enable(void)
{
        /* enable interrupt */
        TIMSK1 |= _BV(OCIE1A);
}

void
timer1_disable(void)
{
        /* disable interrupt */
        TIMSK1 &= ~_BV(OCIE1A);
}

void
timer2_init(uint8_t prescaler, uint8_t ticks)
{
        /* Output Compare pins disconnected */
        TCCR2A = 0;

        /* Turn on CTC mode */
        TCCR2B = _BV(WGM12);

        /* convert prescaler index to TCCRnB prescaler bits CS10, CS11, CS12 */
        TCCR2B |= prescaler;

        OCR2A = ticks;

}


void
timer2_enable(void)
{
        /* enable interrupt */
        TIMSK2 |= _BV(OCIE2A);
}

void
timer2_disable(void)
{
        /* disable interrupt */
        TIMSK2 &= ~_BV(OCIE2A);
}

void
timer0_init(uint8_t prescaler, uint8_t ticks)
{
        /* Output Compare pins disconnected */
        TCCR0A = 0;

        /* Turn on CTC mode */
        TCCR0B = _BV(WGM12);

        /* convert prescaler index to TCCRnB prescaler bits CS10, CS11, CS12 */
        TCCR0B |= prescaler;

        OCR0A = ticks;

}

void
timer0_enable(void)
{
        /* enable interrupt */
        TIMSK0 |= _BV(OCIE0A);
}

void
timer0_disable(void)
{
        /* disable interrupt */
        TIMSK0 &= ~_BV(OCIE0A);

}


uint32_t
micros(void)
{
        return __micros;
}
