#ifndef __BB_H
#define __BB_H

#define BB_SRAM_REF      0x20000000
#define BB_SRAM_BASE     0x22000000

#define BB_PERI_REF      0x40000000
#define BB_PERI_BASE     0x42000000

static inline volatile uint32_t *
bb_addr(volatile void *addr, uint32_t bit, uint32_t bb_base, uint32_t bb_ref)
{
        uint32_t __addr = bb_base + ((uint32_t) addr - bb_ref) * 32 + bit * 4;

        return (volatile uint32_t *) __addr;
}

static inline uint8_t
bb_sram_get_bit(volatile void *addr, uint32_t bit)
{
        return *bb_addr(addr, bit, BB_SRAM_BASE, BB_SRAM_REF);
}

static inline void
bb_sram_set_bit(volatile void *addr, uint32_t bit, uint8_t v)
{
        *bb_addr(addr, bit, BB_SRAM_BASE, BB_SRAM_REF) = v;
}

static inline uint8_t
bb_periph_get_bit(volatile void *addr, uint32_t bit)
{
        return *bb_addr(addr, bit, BB_PERI_BASE, BB_PERI_REF);
}

static inline void
bb_periph_set_bit(volatile void *addr, uint32_t bit, uint8_t v)
{
        *bb_addr(addr, bit, BB_PERI_BASE, BB_PERI_REF) = v;
}

#endif /* __BB_H */
