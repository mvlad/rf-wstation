#ifndef __COMMON_H
#define __COMMON_H

#define ENABLE_EXT_INTERRUPT()  \
        EICRA |= 1;             \
        EIMSK |= _BV(INT0)      \

#define DISBLE_EXT_INTERRUPT()  \
        EIMSK &= ~_BV(INT0)

#endif
