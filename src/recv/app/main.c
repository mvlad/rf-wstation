#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "stm32f10x.h"

#include "app/stm32.h"
#include "app/delay.h"
#include "app/lcd-usart.h"
#include "app/pin.h"
#include "app/rb.h"
#include "app/usart.h"
#include "app/rf.h"
#include "app/bb.h"
#include "app/main.h"
#include "app/compiler.h"

#define dprintf(format, args...)        \
        lcd_clear();                    \
        printf(format, ##args);         \
        _delay_ms(500)

/*
 * how long to wait for receiving data
 */
#define DELAY_WAIT_FOR_DATA     500

/*
 * usart
 */
USART_InitTypeDef usart1;
USART_InitTypeDef usart2;

/*
 * ring buffers 
 */
struct usart_dev usart1_dev;
struct usart_dev usart2_dev;

struct rb usart1_rb;
struct rb usart2_rb;

/*
 * Timer 
 *
 * We use a value of 5000 to reload the general counter
 * TIMER2. 
 *
 * The counter will count upwards from 0 to CCR1 value, with
 * a frequency determined by the prescaler. This gives the period
 * of the counter.
 *
 * The prescaler is initialized with a value of 7199. 
 *
 * prescaler + 1 = SystemCoreClock / Timer period. If we choose 7199 =>
 * X = 72MHz/7.2kHz = 10kHz the period of the counter, which
 * in turns means with the timer reaches 5000 in 0.5s (10kHz / 5k = 2Hz)
 * 
 */
TIM_TimeBaseInitTypeDef timer2;
TIM_OCInitTypeDef       timer2_oc;

/* reload value with 5000 */
__IO uint16_t CCR1_Val = 10000;

/* prescaler determined above */
uint16_t prescaler = 7199;

/* storage for uptime clock */
volatile struct clock clk_uptime;

/*
 * storage for local time clock
 */
volatile struct clock clk_clock;

/*
 * ticker for local time
 */
volatile uint32_t jiffies = JIFFIES;

/*
 * ticker for systick 
 */
volatile uint32_t ticks = 0;

/*
 * We get the data in a 4-byte pack. 
 *
 * This method converts to a more meaningful interpretation value.
 */
uint32_t
recover_data(uint8_t *d, uint8_t len)
{
        int32_t data = 0;

        uint8_t i;

        for (i = 0; i < len; i++) {
                data |= d[i];
                if (i < (len - 1))
                        data <<= 8;
        }

        return data;
}

/*
 * Init the flash memory
 */
void
flash_init(void)
{
        FLASH_PrefetchBufferCmd(FLASH_PrefetchBuffer_Enable);
        FLASH_SetLatency(FLASH_Latency_2);
}

/*
 * Init the usart. The procedure is the same usart2
 */
void
usart1_init(uint16_t rx_pin, uint16_t tx_pin, uint32_t bauds)
{
        NVIC_InitTypeDef nvic;
        GPIO_InitTypeDef gpio;

        /* configure the clock */
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1 | 
                               RCC_APB2Periph_AFIO | 
                               RCC_APB2Periph_GPIOA, ENABLE);

        /* Configure the NVIC Preemption Priority Bits */  
        NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);
        nvic.NVIC_IRQChannel = USART1_IRQn;
        nvic.NVIC_IRQChannelSubPriority = 0;
        nvic.NVIC_IRQChannelCmd = ENABLE;
        NVIC_Init(&nvic);

        /* configure rx and tx pins */
        gpio.GPIO_Pin = tx_pin;
        gpio.GPIO_Speed = GPIO_Speed_50MHz;
        gpio.GPIO_Mode = GPIO_Mode_AF_PP;
        GPIO_Init(USART1_GPIO, &gpio);

        gpio.GPIO_Pin = rx_pin;
        gpio.GPIO_Mode = GPIO_Mode_IN_FLOATING;
        gpio.GPIO_Speed = GPIO_Speed_50MHz;
        GPIO_Init(USART1_GPIO, &gpio);

        /* 
         * working mode: bauds, many bytes a frame has, 
         * if stop bits present and how many, the parity
         */
        usart1.USART_BaudRate = bauds;
        usart1.USART_WordLength = USART_WordLength_8b;
        usart1.USART_StopBits = USART_StopBits_1;
        usart1.USART_Parity = USART_Parity_No;
        usart1.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
        usart1.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
        USART_Init(USART1, &usart1);

        /* enable rx interrupts */
        USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);

        USART_Cmd(USART1, ENABLE);
}

void
usart2_init(uint16_t rx_pin, uint16_t tx_pin, uint32_t bauds)
{
        GPIO_InitTypeDef gpio;
        NVIC_InitTypeDef nvic;

        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | 
                               RCC_APB2Periph_AFIO, ENABLE);

        RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);

        NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);
        nvic.NVIC_IRQChannel = USART2_IRQn;
        nvic.NVIC_IRQChannelSubPriority = 1;
        nvic.NVIC_IRQChannelCmd = ENABLE;
        NVIC_Init(&nvic);

        gpio.GPIO_Pin = rx_pin;
        gpio.GPIO_Mode = GPIO_Mode_IN_FLOATING;
        gpio.GPIO_Speed = GPIO_Speed_50MHz;
        GPIO_Init(USART2_GPIO, &gpio);  

        gpio.GPIO_Pin = tx_pin;
        gpio.GPIO_Speed = GPIO_Speed_50MHz;
        gpio.GPIO_Mode = GPIO_Mode_AF_PP;
        GPIO_Init(USART2_GPIO, &gpio);  

        usart2.USART_BaudRate = bauds;
        usart2.USART_WordLength = USART_WordLength_8b;
        usart2.USART_StopBits = USART_StopBits_1;
        usart2.USART_Parity = USART_Parity_No;
        usart2.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
        usart2.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
        USART_Init(USART2, &usart2);

        USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);

        USART_Cmd(USART2, ENABLE);
}

void
ms_to_clock(volatile struct clock *date, uint32_t ms)
{       
        uint32_t secs = ms / 1000;
        uint32_t mins = 0;
        uint32_t hours = 0;


        if (secs >= 60) {
                mins = secs / 60;
                secs = secs % 60;
        }

        if (mins >= 60) {
                hours = mins / 60;
                mins = mins % 60;
        }

        if (hours >= 24)
                hours = hours % 24;

        date->secs = secs;
        date->mins = mins;
        date->hours = hours;
}

void
rtc_time(volatile struct clock *date, uint32_t secs)
{
        if (secs == 0x0001517f) {
                RTC_SetCounter(0x0);
                RTC_WaitForLastTask();
                date->hours = date->mins = date->secs = 0;
        } else {
                date->hours = (uint8_t ) (secs / 3600);
                date->mins = (uint8_t) ((secs % 3600) / 60);
                date->secs = (uint8_t) ((secs % 3600) % 60);
        }
}

void
timer_run_cb(void)
{
        /* toggle the led */
        board_pin_toggle(GPIOA, GPIO_Pin_1);

        /* get the current seconds */
        jiffies = RTC_GetCounter();
#if 0
        GPIO_WriteBit(GPIOA, GPIO_Pin_1, 
                (BitAction)(1 - GPIO_ReadOutputDataBit(GPIOA, GPIO_Pin_1)));
#endif


        /* update both the uptime and current time */
        ms_to_clock(&clk_uptime, ticks);
        rtc_time(&clk_clock, jiffies);
}

void
timer_init(void (*cb)(void))
{
        NVIC_InitTypeDef nvic;

        /* Enable the TIM2 global Interrupt */
        nvic.NVIC_IRQChannel = TIM2_IRQn;
        nvic.NVIC_IRQChannelPreemptionPriority = 0;
        nvic.NVIC_IRQChannelSubPriority = 1;
        nvic.NVIC_IRQChannelCmd = ENABLE;
        NVIC_Init(&nvic);

        /* Clock configuration */

        /* GPIOC clock enable */
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);

        /* TIM2 clock enable */
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);


        /* Time base configuration */
        timer2.TIM_Period = 65535;
        timer2.TIM_Prescaler = 0;
        timer2.TIM_ClockDivision = 0;
        timer2.TIM_CounterMode = TIM_CounterMode_Up;

        TIM_TimeBaseInit(TIM2, &timer2);

        /* Prescaler configuration */
        TIM_PrescalerConfig(TIM2, prescaler, TIM_PSCReloadMode_Immediate);

        /* Output Compare Timing Mode configuration: Channel1 */
        timer2_oc.TIM_OCMode = TIM_OCMode_Timing;
        timer2_oc.TIM_OutputState = TIM_OutputState_Enable;
        timer2_oc.TIM_Pulse = CCR1_Val;
        timer2_oc.TIM_OCPolarity = TIM_OCPolarity_High;

        TIM_OC1Init(TIM2, &timer2_oc);
        TIM_OC1PreloadConfig(TIM2, TIM_OCPreload_Disable);

        /* TIM IT enable */
        TIM_ITConfig(TIM2, TIM_IT_CC1, ENABLE);

        /* TIM2 enable counter */
        TIM_Cmd(TIM2, ENABLE);

        /* callback run in the interrupt handler */
        timer2_cb = cb;
}

void
rtc_config(void)
{
        NVIC_InitTypeDef nvic;

        /* Configure one bit for preemption priority */
        NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);

        /* Enable the RTC Interrupt */
        nvic.NVIC_IRQChannel = RTC_IRQn;
        nvic.NVIC_IRQChannelPreemptionPriority = 0;
        nvic.NVIC_IRQChannelSubPriority = 1;
        nvic.NVIC_IRQChannelCmd = ENABLE;
        NVIC_Init(&nvic);

        /* Enable PWR and BKP clocks */
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR | RCC_APB1Periph_BKP, 
                               ENABLE);

        /* Allow access to BKP Domain */
        PWR_BackupAccessCmd(ENABLE);

        /* Reset Backup Domain */
        BKP_DeInit();

        /* Enable LSE */
        RCC_LSEConfig(RCC_LSE_ON);

        /* Wait till LSE is ready */
        while (RCC_GetFlagStatus(RCC_FLAG_LSERDY) == RESET)
                ;

        /* Select LSE as RTC Clock Source */
        RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE);

        /* Enable RTC Clock */
        RCC_RTCCLKCmd(ENABLE);

        /* Wait for RTC registers synchronization */
        RTC_WaitForSynchro();

        /* Wait until last write operation on RTC registers
         * has finished */
        RTC_WaitForLastTask();

        /* Enable the RTC Second */
        RTC_ITConfig(RTC_IT_SEC, ENABLE);

        /* Wait until last write operation on RTC
         * registers has finished */
        RTC_WaitForLastTask();

        /* Set RTC prescaler: set RTC period to 1sec */
        RTC_SetPrescaler(32767); /* RTC period = RTCCLK/RTC_PR = (32.768 KHz)/(32767+1) */

        /* Wait until last write operation on RTC
         * registers has finished */
        RTC_WaitForLastTask();

}

void
rtc_init(uint32_t ms)
{
        if (BKP_ReadBackupRegister(BKP_DR1) != 0xA5A5) {


                rtc_config();

                RTC_WaitForLastTask();
                RTC_SetCounter(ms);
                RTC_WaitForLastTask();

                BKP_WriteBackupRegister(BKP_DR1, 0xA5A5);
        } else {
                /* 
                 * no need to reconfigure 
                 */

                /* Wait for RTC registers synchronization */
                RTC_WaitForSynchro();

                /* Enable the RTC Second */
                RTC_ITConfig(RTC_IT_SEC, ENABLE);

                /* Wait until last write operation on RTC registers has
                 * finished */
                RTC_WaitForLastTask();
        }

        /* Clear reset flags */
        RCC_ClearFlag();
}

void
show_clock(void)
{
        /* show the clock/uptime */
        lcd_clear();
        printf("U: %.2u:%.2u:%.2u", 
                clk_uptime.hours, clk_uptime.mins, 
                clk_uptime.secs); 

        lcd_set_cursor(2, 1);
        printf("C: %.2u:%.2u:%.2u", 
                clk_clock.hours, clk_clock.mins, 
                clk_clock.secs); 
}

void
show_temp_pressure(struct bmp bmp)
{
        if (bmp.temp && bmp.pressure) {

                lcd_clear();
                printf("T: %.1f (C)  ", bmp.temp / 10.0);

                lcd_set_cursor(2, 1);
                printf("P: %.1f (hPa)", bmp.pressure / 100.0);
        } else {
                lcd_clear();
                printf("Data fail");
        }
}

void
get_temp_pressure(struct bmp *bmp)
{
        int recv = -1;

        uint8_t rbuf[4];
        uint8_t temp_data = 1;

        int32_t tmp;

read_again:
        memset(rbuf, 0, sizeof(uint8_t) * 4);

        recv = rf_recv_mbytes(&usart2_dev, rbuf, 4, 1);

        if (recv == 0) {
                if (temp_data) {
                        bmp->temp = recover_data(rbuf, 4);
                } else  {
                        bmp->pressure = recover_data(rbuf, 4);
                }

                temp_data ^= 1;

                /* go one more time to get the pressure */
                if (bmp->temp && !bmp->pressure) {
                        _delay_ms(DELAY_WAIT_FOR_DATA);
                        goto read_again;
                }
        } else {
                bmp->temp = 0;
                bmp->pressure = 0;
        }

        /* must swap the value(s) in case of resets  */
        if (bmp->temp && bmp->pressure) {
                if (bmp->temp > 5000) {

                        tmp = bmp->temp;
                        bmp->temp = bmp->pressure;
                        bmp->pressure = tmp;
                }
        }
}

void
pins_init(void)
{
        /* green led */
        pin_init(GPIOA, OUTPUT, RCC_APB2Periph_GPIOA, GPIO_Pin_5);

        /* yellow led, err led */
        pin_init(GPIOA, OUTPUT, RCC_APB2Periph_GPIOA, GPIO_Pin_1);

        /* input pin */
        pin_init(GPIOC, INPUT, RCC_APB2Periph_GPIOC, GPIO_Pin_9);
}

void
usart_init(void)
{
        /* init the ring buffer for serial */
        dev_init(&usart1_dev, USART1, &usart1_rb);
        dev_init(&usart2_dev, USART2, &usart2_rb);

        /* init usart for display data on serial lcd */
        usart1_init(GPIO_Pin_10, GPIO_Pin_9, 9600);

        /* for receiving RF data */
        usart2_init(GPIO_Pin_3, GPIO_Pin_2, 1200);
}

void
setup(void)
{
        /* flash configuration */
        flash_init();

        pins_init();

        /* every ms tick */
        SysTick_Config((SystemCoreClock / 1000) - 1);

        /* init RTC */
        rtc_init(jiffies);

        /* init TIMR2 */
        timer_init(timer_run_cb);

        /* RF and display */
        usart_init();

        /* disable buffering for printf */
        setvbuf(stdout, NULL, _IONBF, 0);

        lcd_init();
        _delay_ms(200);

        printf("Setup finished");
        _delay_ms(200);

        lcd_clear();
}

void
loop(void)
{
        uint8_t time_display = 0;
        struct bmp bmp;

        /* a counter used for switching what to display */
        uint32_t cnt = 0;

        while (1) {

                board_pin_toggle(GPIOA, GPIO_Pin_5);

                memset(&bmp, 0, sizeof(struct bmp));

                /* get data */
                get_temp_pressure(&bmp);

                /* if two seconds have passed */
                if (cnt && ((cnt % 2) == 0)) {

                        /* show the temp/pressure */
                        if (!time_display) {
                                show_temp_pressure(bmp);
                        } else {
                                /* show the clock */
                                show_clock();
                        }

                        /* switch display */
                        time_display ^= 1; 
                }
                cnt++;
                _delay_ms(1000);
        }
}

int main(void)
{
        setup();
        loop();

        /* never here */
        return 0;
}
