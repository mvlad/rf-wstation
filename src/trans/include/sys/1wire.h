#ifndef __1WIRE_H
#define __1WIRE_H

#define     DELAY_A_STD_MODE    6
#define     DELAY_B_STD_MODE    64

#define     DELAY_C_STD_MODE    60
#define     DELAY_D_STD_MODE    10

#define     DELAY_E_STD_MODE    9
#define     DELAY_F_STD_MODE    55

#define     DELAY_H_STD_MODE    480
#define     DELAY_I_STD_MODE    70

#define     DELAY_J_STD_MODE    410

#define RELEASE_BUS()           \
        DDRD &= ~_BV(DDD2);     \
        PORTD &= ~_BV(PIND2)    \

#define PUT_BUS_LOW()           \
        DDRD |= _BV(DDD2);      \
        PORTD &= ~_BV(PIND2)    \

#define PUT_BUS_HIGH()          \
        DDRD |= _BV(DDD2);      \
        PORTD |= _BV(PIND2)     \

#define OW_BIT                  \
        (PIND & (1 << PIND2))   \

#define ERR_TIMEOUT             0x01
#define ERR_TIMEOUT_READ        0x02
#define ERR_TIMEOUT_WRITE       0x03

#define RETRY_MAX_COUNT 96
#define RETRY_MAX_COUNT_READ 1080

/*
 * master ops
 */
extern void
owm_init(void);

extern uint8_t
owm_detect_presence(void);

extern uint8_t
owm_read_bit(void);

extern uint8_t
owm_read_byte(void);

extern void
owm_write_bit(uint8_t b);

extern void
owm_write_byte(uint8_t byte);

/* 
 * slave ops
 */
extern uint8_t
ows_detect_reset(void);

extern uint8_t
ows_send_presence(void);

extern void 
ows_write_bit(uint8_t bit);

extern void
ows_write_byte(uint8_t byte);

extern uint8_t
ows_read_bit(void);

extern uint8_t
ows_read_byte(void);

#endif /* __1WIRE_H */
