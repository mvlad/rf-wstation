#include <avr/io.h>
#include <util/delay.h>

#include "sys/spi.h"
#include "sys/printfmt.h"
#include "app/7segment.h"

void 
ss_set_brightness(uint8_t val)
{
        /* Set brightness command byte */
        spi_send_command(SEVEN_SEGMENT_COMMAND_BRIGHTNESS, val);
}

/*
 * Send the clear display command (0x76)
 * This will clear the display and reset the cursor
 */
void
ss_clear_display(void)
{
        spi_send_simple(SEVEN_SEGMENT_COMMAND_CLEAR);
}

void 
ss_set_decimals(uint8_t val)
{
        spi_send_command(SEVEN_SEGMENT_COMMAND_DECIMALS, val);
}


void 
ss_send_str(char *str)
{
        int i;

        for (i = 0; i < 4; i++) {
                spi_send_simple(str[i]);
        }
}

void
ss_write_no(uint32_t no)
{
        uint8_t digits[4] = { 0x00, 0x00, 0x00, 0x00 };
        uint8_t i = 0;

        if (no > 9999)
                no = 0;

        while (no > 0) {
                digits[i++] = no % 10;
                no = no / 10;
        }

        spi_send_simple(digits[3]);
        spi_send_simple(digits[2]);

        spi_send_simple(digits[1]);
        spi_send_simple(digits[0]);
}

void
ss_write_data(int8_t quot, int8_t rem)
{
        uint8_t p[2] = { 0x00, 0x00 };
        uint8_t i = 0;

        /* set the decimal, the 2 bit must be on, ABCD -> ABC.D */
        ss_set_decimals(1 << 2);

        spi_send_simple(0x00);

        while (quot > 0) {
                p[i++] = quot % 10;
                quot /= 10;
        }

        spi_send_simple(p[1]);
        spi_send_simple(p[0]);

        spi_send_simple(rem);
}

void
ss_write_date(volatile struct clock *clk)
{
        uint8_t p[2] = { 0x00, 0x00 };
        uint8_t i = 0;

        uint8_t hrs = clk->hrs;
        uint8_t mins = clk->mins;

        while (hrs) {
                p[i++] = hrs % 10;
                hrs /= 10;
        }

        spi_send_simple(p[1]);
        spi_send_simple(p[0]);

        p[1] = 0x00;
        p[0] = 0x00;

        i = 0;
        while (mins) {
                p[i++] = mins % 10;
                mins /= 10;
        }

        spi_send_simple(p[1]);
        spi_send_simple(p[0]);
}


void
ss_setup(void)
{
        ss_set_brightness(1);
        _delay_ms(10);

        ss_write_no(1111);
        _delay_ms(20);

        ss_clear_display();
        _delay_ms(150);

        ss_write_no(9999);
        _delay_ms(100);
}
