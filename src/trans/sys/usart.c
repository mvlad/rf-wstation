#include <avr/io.h>
#include "sys/usart.h"

/*
 * 1200 bauds, 8-bit data, 1 bit stop
 *
 * BAUD  = F_OSC / (16 * UBBR0 + 1)
 * UBBR0 = (F_OSC / 16 * BAUDS) - 1
 */
void
usart_init(void)
{
        /* transmit and receive */
        UCSR0B = (1 << TXEN0) | (1 << RXEN0);

        /* 8 bits, 1 bit stop no parity */
        UCSR0C = (1 << UCSZ00) | (1 << UCSZ01) | (1 << UMSEL01);

        /* UBRR0 = 0x0033; */

        /* 1200 bauds, 415 = (8M / 16 * 1200) - 1 */
        UBRR0 = 0x019f;
}

int
usart_send_byte(uint8_t byte)
{
        /* wait until tranmit buffer has finished */
        while (!(UCSR0A & (1 << UDRE0)))
                ;

        UDR0 = byte;
        return 0;
}

int
usart_send_ch(unsigned char ch)
{
        if (ch == '\n')
                usart_send_ch('\r');

        while (!(UCSR0A & (1 << UDRE0)))
                ;
        UDR0 = ch;

        return 0;
}

uint8_t
usart_recv_ch(void)
{
        uint8_t r;

        /* wait until receive buffer has finished */
        while (!(UCSR0A & (1 << RXC0)))
                ;

        r = UDR0;
        return r;
}

void
usart_flush_rx(void)
{
        uint8_t d;

        while (!(UCSR0A & (1 << RXC0)))
                ;

        d = UDR0;
        (void) d;
}
