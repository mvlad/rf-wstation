#include "stm32f10x.h"

#include "app/compiler.h"
#include "app/pin.h"

#define NBASE   ((NVIC_Type *) NVIC_BASE)

static inline void
nvic_disable_all(void)
{
        NBASE->ICER[0] = 0xffffffff;
        NBASE->ICER[1] = 0xffffffff;
}

static inline void
timer_disable_all(void)
{
        TIM_DeInit(TIM1);
        TIM_DeInit(TIM2);
        TIM_DeInit(TIM3);
        TIM_DeInit(TIM4);
}

static inline void
usart_disable_all(void)
{
        USART_DeInit(USART1);
        USART_DeInit(USART2);
}

static inline __attribute__ ((noreturn)) void
fade_in_out(void)
{
        uint32_t i = 0;
        int32_t s = 0;

        uint16_t TOP_CNT = 0x0400;
        uint16_t j = 0x0000;

        /* yellow led, err led */
        pin_init(GPIOA, OUTPUT, RCC_APB2Periph_GPIOA, GPIO_Pin_1);
        board_pin_off(GPIOA, GPIO_Pin_1);

        for (;;) {

                if (j == TOP_CNT)
                        s = -1;
                if (j == 0)
                        s = 1;

                if (i == TOP_CNT) {
                        j += s; 
                        i = 0;
                }

                if (i < j)
                        board_pin_on(GPIOA, GPIO_Pin_1);
                else
                        board_pin_off(GPIOA, GPIO_Pin_1);
                i++;
        }

}

/*
 * exceptions for sys/exc.S will land here with interrupts disabled
 */
__attribute__ ((noreturn))
void __error(void)
{
        nvic_disable_all();

        timer_disable_all();

        usart_disable_all();

        /* enable interrupts */
        asm volatile("cpsie i");

        fade_in_out();

}
