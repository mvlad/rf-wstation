#ifndef __PIN_H
#define __PIN_H

/*
 * configure the pin as input or output
 */
enum pin_mode {
        INPUT = 1,
        OUTPUT = 2,
};

#define ERROR_LED_PIN   BOARD_ERROR_LED_PIN

/*
 * turn on a pin
 */
extern void
board_pin_on(GPIO_TypeDef *GPIO_PORT, uint16_t led);

/*
 * turn off a pin
 */
extern void
board_pin_off(GPIO_TypeDef *GPIO_PORT, uint16_t led);

/*
 * toggle a pin 
 */
extern void
board_pin_toggle(GPIO_TypeDef *GPIO_PORT, uint16_t led);

/**
 * initialize a pin
 */
extern void
pin_init(GPIO_TypeDef *GPIO_PORT,
         enum pin_mode type, uint32_t clk, uint16_t led);

/*
 * macros for easy usage. All PINs are on GPIO port
 */

#define PIN_ON(PIN)     \
        board_pin_on(GPIOA, PIN)

#define PIN_OFF(PIN)     \
        board_pin_on(GPIOA, PIN)

#define TOGGLE_PIN(PIN) \
        board_pin_toggle(GPIOA, PIN)
        

#endif 
