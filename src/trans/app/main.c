#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <errno.h>

#include "sys/pins.h"
#include "sys/usart.h"
#include "sys/printfmt.h"

#include "sys/i2c.h"
#include "sys/spi.h"

#include "sys/exti.h"
#include "sys/timer.h"
#include "sys/eeprom.h"

#include "app/7segment.h"
#include "app/rf.h"
#include "app/bmp085.h"


/*
 * store data from BMP
 */
volatile struct bmp_data bmp_data;

/*
 * var to display the clock or the temp
 */
volatile uint8_t display_temp = 0;

/*
 * we send first the temp, then the pressure
 */
volatile uint8_t send_temp = 1;

/*
 * store clock info here
 */
volatile struct clock clk; 

/*
 * ticker, store initial value here, can be modified by triggering
 * a INT interrupt
 */
volatile uint32_t jiffies = JIFFIES;

/*
 * working mode, to switch between displaying 
 * the clock or the temp 
 */
volatile uint8_t normal_mode = 1;

/*
 * we need to convert between a int32_t, uint32_t to 4 bytes 
 * to send it to serial
 */
static void
no_to_bytes(uint8_t *d, int32_t no)
{
        *d++ = (no >> 24) & 0xff;
        *d++ = (no >> 16) & 0xff;
        *d++ = (no >> 8) & 0xff;
        *d++ = no & 0xff;
}

/*
 * convert between jiffies to a clock format
 */
static __inline void
ms_to_clock(volatile struct clock *clk, uint32_t ms)
{ 
        uint32_t secs = ms / 1000;
        uint32_t mins = 0;
        uint32_t hours = 0;

        if (secs >= 60) {
                mins = secs / 60;
                secs = secs % 60;
        }

        if (mins >= 60) {
                hours = mins / 60;
                mins = mins % 60;
        }

        if (hours >= 24)
                hours = hours % 24;

        clk->secs = secs;
        clk->mins = mins;
        clk->hrs = hours;
}

/*
 * convert from jiffies to clock representation
 */
static __inline uint32_t
clock_to_ms(volatile struct clock c)
{
        uint32_t secs = 0UL;
        uint32_t ms = 0UL;

        /* use 0UL to force the compiler to treat it as unsigned long */
        secs = (c.hrs * 3600UL) + (c.mins * 60UL) + c.secs;
        ms = secs * 1000UL;

        return ms;
}

/*
 *
 * init all protocols required
 */
static void
proto_init(void)
{
        /* the usart is responsible for sending the data remotly */
        usart_init();
        
        /* TWI interface for communication with BMP */
        i2c_init();

        /* SPI for communcation with 7-segment display */
        spi_init();
}

/*
 * The interrupt is used to change the main program when
 * displaying either the current time and the temp or setting up
 * the time.
 *
 * A button should be hooked on PORTD port, D2 pin.
 */
ISR(INT0_vect)
{
        /* FIXME 
         * check if indeed the compiler disables the interrupts
         */
        DISBLE_EXT_INTERRUPT();

        if ((PIND & (1 << PIND2)) == 0) {

                /* small de-bounce */
                _delay_ms(1);

                while ((PIND & (1 << PIND2)) == 0)
                        ;

                /* switch the mode */
                normal_mode ^= 1;

                /*
                 * do not count the jiffies when modifying the time
                 */
                if (normal_mode) {
                        /* make sure we convert back to jiffies */
                        jiffies = clock_to_ms(clk);

                        /* enable the counter */
                        timer1_enable();

                        PIN_OFF(D, 6);
                } else {
                        /* signal that by turning on the LED */
                        PIN_ON(D, 6);

                        /* disable the counter, we're now in programming mode */
                        timer1_disable();
                }

        }

        ENABLE_EXT_INTERRUPT();
}

/*
 * send data over RF
 */
ISR(TIMER1_COMPA_vect)
{
        uint8_t data[4];
        toggle_PIN(D, 5);

        /* take a sample of the temp/pressure */
        bmp_get_all(&bmp_data);

        /* adjust the temp */
        bmp_data.temp -= 50;

        data[0] = 0x00; data[1] = 0x00;
        data[2] = 0x00; data[3] = 0x00;

        /*
         * we first send the temperature and when the next overflow interrupt 
         * of the timer we send the pressure. 
         *
         * The received is responbile retrieve two 8-byte frames and to
         * decide which is which. Given that the pressure is an order of
         * magitude higher it doesn't really matter the order as the receiver
         * can easily distinguish between them.
         */
        if (send_temp) {
                no_to_bytes(data, bmp_data.temp);
                rf_send_frame_bytes(data, 4, 1);
        } else {
                no_to_bytes(data, bmp_data.pressure);
                rf_send_frame_bytes(data, 4, 1);
        }

        send_temp ^= 1;

        /* update clock */
        ms_to_clock(&clk, jiffies);
        jiffies += 500;
}


static void
setup(void)
{
        /* setup pins, buttons as inputs and two leds for usage*/
        pins_init();

        /* timer 16-bit with 0.5s delay */
        timer1_init(4, 15625);

        /* enable the timer interrupt */
        timer1_enable();

        /* initialize protocols for communication with devices */
        proto_init();

        /* init the 7-segment display */
        ss_setup();

        /* init the temp sensor */
        bmp_init(BMP_STANDARD);

        /* init the data storage */
        bmp_data.temp = 0;
        bmp_data.pressure = 0;

        /* enable external interrupts */
        ENABLE_EXT_INTERRUPT();

        /* enable global interrupts */
        sei();
}

/*
 * Timer1 is responsible for retrieve data from BMP
 * Timer2 is responsible for ticking, updating clock
 *
 * INT0 is responsible for disableing Timer2 and allowing
 * to configure the clock.
 *
 * The main loop takes care of display the data to the 
 * seven segment display.
 */

static void
loop(void)
{
        while (1) {
                ss_set_brightness(1);

                /*
                 * In normal mode we display first the temp then
                 * the clock. This cycling happens every two seconds.
                 *
                 * If not in normal mode, we switch to ``programming'' 
                 * mode in order to adjust the clock.
                 *
                 * Note that it will take ``a while'' betweening switching.
                 */
                if (normal_mode) {
                        /* just signal we're running */
                        toggle_PIN(D, 6);

                        if (display_temp) {
                                ss_clear_display();

                                /* activate the column */
                                ss_set_decimals(1 << 4);
                                ss_write_date(&clk);
                        } else {
                                if (bmp_data.temp && bmp_data.pressure) {

                                        /* adjust the values */
                                        int8_t t = bmp_data.temp / 10;
                                        int8_t tr = bmp_data.temp % 10;

                                        ss_clear_display();
                                        ss_write_data(t, tr);
                                }
                        }

                        /* switch the diplay */
                        display_temp ^= 1;
                        _delay_ms(2000);

                } else {
                        /* 
                         * A button should be hooked on PORTD port, D3 pin
                         * A button should be hooked on PORTD port, D4 pin
                         *
                         * Responsible for setting up the clock
                         *
                         * Note that we convert back to jiffies when the we
                         * get out of ``programming'' mode.
                         */

                        /* setting up the minutes */
                        if ((PIND & (1 << PIND3)) == 0) {
                                /* de-bouncer */
                                _delay_ms(1);

                                while((PIND & (1 << PIND3)) == 0)
                                        ;

                                clk.mins++;

                                if (clk.mins == 60) {
                                        clk.hrs++;

                                        if (clk.hrs == 24) {
                                                clk.hrs = clk.hrs % 24;
                                        }
                                        /* reset mins */
                                        clk.mins = 0;
                                }
                        } else  if ((PIND & (1 << PIND4)) == 0) { /* hours */
                                /* de-bouncer */
                                _delay_ms(1);

                                while((PIND & (1 << PIND4)) == 0)
                                        ;

                                clk.hrs++;
                                if (clk.hrs == 24) {
                                        clk.hrs = clk.hrs % 24;
                                }
                        }

                        ss_clear_display();

                        ss_set_decimals(1 << 4);
                        ss_write_date(&clk);

                        /* short delay */
                        _delay_ms(50);
                }
        }
}

int main(void)
{
        setup();
        loop();

        return 0;
}
