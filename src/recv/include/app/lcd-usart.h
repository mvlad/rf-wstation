#ifndef __SERLCD_USART_H
#define __SERLCD_USART_H

#define COMMAND_LCD_INIT                0x7c    /* special command */

#define PWM_OFF                         128     /* turn off lcd */

#define PWM_4                           140     /* 40% brightness */
#define PWM_7                           150     /* 70% brightness */
#define PWM_FULL                        157     /* 100% brightness */

#define SPLASH                          0x0a

#define COMMAND_LCD_CONTROL             0xfe    /* command */

#define CLEAR_LCD                       0x01
#define RETURN_HOME                     0x02

#define TURN_VISUAL_ON                  0x0c
#define TURN_VISUAL_OFF                 0x08

#define SCROLL_RIGHT                    0x1c
#define SCROLL_LEFT                     0x18

#define MOVE_CURSOR_LEFT                0x00
#define MOVE_CURSOR_RIGHT               0x02


#define COMMAND_START_SET_CURSOR        0x80

#define MAX_CURSOR                      32
#define START_CURSOR                    128

/*
 * initialize SerLCD display
 */
extern void 
lcd_init(void);

/*
 * clear the display
 */
extern void
lcd_clear(void);

/*
 * put the cursor at the start 
 */
extern void
lcd_return_home(void);

/*
 * set cursor at different position
 */
extern void
lcd_set_cursor(uint8_t row, uint8_t col);

/*
 * slide the text to right
 */
extern void
lcd_left_to_right(void);

#endif /* __SERLCD_USART_H */
