#ifndef __PRINTFTM_H
#define __PRINTFTM_H

typedef __builtin_va_list va_list;

#ifndef va_start
#define va_start(ap, last)      __builtin_va_start(ap, last)
#endif
#ifndef va_arg
#define va_arg(ap, type)        __builtin_va_arg(ap, type)
#endif

#ifndef va_end
#define va_end(ap)              __builtin_va_end(ap)
#endif

#define LARGE_DOUBLE_THR (9.1e18)

#ifndef ABS
#define ABS(x) (((x) > 0) ? (x) : -(x))
#endif

int 
do_printf(const char *fmt, ...);

#ifdef DEBUG
#define dprintf(format, args...) do {   \
        do_printf(format, ##args);      \
} while (0)
#else
#define dprintf(format, args...) { }
#endif

#endif /* __PRINTFTM_H */
