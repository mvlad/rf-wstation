#include <stdint.h>
#include <string.h>

#include "sys/stm32f10x_conf.h"

#include "app/stm32.h"
#include "app/pin.h"
#include "app/rb.h"
#include "app/usart.h"
#include "app/rf.h"

static uint8_t 
compute_crc8(uint8_t *frames, uint8_t len)
{
        uint8_t crc = 0;
        uint8_t i, inb, mix;

        while (len--) {
                inb = *frames++;

                for (i = 8; i; i--) {

                        mix = (crc ^ inb);
                        mix = mix & 0x01;

                        crc >>= 1;
                        if (mix)
                                crc ^= 0x8c;

                        inb >>= 1;
                }
        }
        return crc;
}

int
rf_recv_byte(struct usart_dev *dev, uint8_t *data, int wait)
{
        int pos;

        uint8_t i, recv_data = 0x00;
        uint8_t frames_recv;

        uint8_t header_frame[RF_FRAME_HEADER_SIZE] = { 
                RF_START0, RF_START1, RF_DST
        };

        /* what we get over the wire */
        uint8_t frame[RF_FRAME_SIZE];

        frames_recv = usart_data_available(dev);
        if (frames_recv < RF_FRAME_SIZE) {
                PIN_ON(ERROR_LED_PIN);
                return -1;
        }

        PIN_OFF(ERROR_LED_PIN);

        pos = 0;

        memset(frame, 0, RF_FRAME_SIZE);

        while (pos < RF_FRAME_HEADER_SIZE) {

                while (!usart_data_available(dev)) {
                        TOGGLE_PIN(BOARD_LED_PIN);
                }

                i = usart_getc(dev);

                if (i == header_frame[pos]) {

                        /* 
                         * alright, we've got the header frame, now
                         * determine the data and the checksum
                         */
                        if (pos == (RF_FRAME_HEADER_SIZE - 1)) {

                                /* the destination */
                                frame[pos++] = i;

                                /* wait for data and crc */
                                while (usart_data_available(dev) < 
                                                RF_FRAME_DATA_SIZE + 
                                                RF_FRAME_CRC_SIZE) {
                                        TOGGLE_PIN(ERROR_LED_PIN);
                                }

                                /* get the data and store it */
                                frame[pos++] = usart_getc(dev);

                                /* get the crc and store it */
                                frame[pos++] = usart_getc(dev);

                                /* verify it */
                                uint8_t crc = compute_crc8(frame, RF_FRAME_SIZE);
                                if (crc == 0) {
                                        /* break and return data */
                                        recv_data = frame[3];
                                        break;
                                } else {
                                        pos = 0;
                                        return -1;
                                }
                        }

                        /* store the header frame */
                        frame[pos++] = i;
                } else {
                        pos = 0;
                        if (!wait) {
                                return -1;
                        }
                }
        }

        *data = recv_data;
        return 0;

}


int
rf_recv_all_bytes(struct usart_dev *dev, uint8_t *dst, uint8_t len)
{
        uint8_t i = 0;

        while (i < len) {
                uint8_t d;

                /* wait until we got something of value */
                while (rf_recv_byte(dev, &d, 1) == -1)
                        ;

                dst[i++] = d;
        }

        if (len == i)
                return 0;
        else
                return -1;
}

int
rf_recv_mbytes(struct usart_dev *dev, uint8_t *data, uint8_t len, int wait)
{
        int pos;

        uint8_t i;
        uint8_t frames_recv;

        uint8_t header_frame[RF_FRAME_HEADER_SIZE] = { 
                RF_START0, RF_START1, RF_DST
        };

        /* what we get over the wire */
        uint8_t frame[RF_FRAME_SIZE_BYTE];
        

        if (len > RF_FRAME_DATA_SIZE_BYTE) {
                return -1;
        }

        frames_recv = usart_data_available(dev);
        if (frames_recv < RF_FRAME_SIZE_BYTE) {
                return -1;
        }

        pos = 0;

        memset(frame, 0, RF_FRAME_SIZE_BYTE);

        while (pos < RF_FRAME_HEADER_SIZE) {

                while (!usart_data_available(dev))
                        ;

                i = usart_getc(dev);

                if (i == header_frame[pos]) {

                        /* 
                         * alright, we've got the header frame, now
                         * determine the data and the checksum
                         */
                        if (pos == (RF_FRAME_HEADER_SIZE - 1)) {

                                /* the destination */
                                frame[pos++] = i;

                                /* wait for data and crc */
                                while (usart_data_available(dev) < 
                                                RF_FRAME_DATA_SIZE_BYTE + 
                                                RF_FRAME_CRC_SIZE)
                                        ;

                                /* get the data and store it */
                                uint8_t p, i;
                                for (p = 0; p < RF_FRAME_DATA_SIZE_BYTE; p++)
                                        frame[pos++] = usart_getc(dev);

                                /* get the crc and store it */
                                frame[pos++] = usart_getc(dev);


                                /* verify it */
                                uint8_t crc;
                                crc = compute_crc8(frame, RF_FRAME_SIZE_BYTE);
                                if (crc == 0) {
                                        /* break and return data */
                                        for (i = 0, p = 0; 
                                             p < RF_FRAME_DATA_SIZE_BYTE && i < len;
                                             p++, i++) {
                                               data[i] = frame[p + 3];     
                                        }
                                        break;
                                } else {
                                        /* reset the frame */
                                        pos = 0;
                                        return -1;
                                }
                        }

                        /* store the header frame */
                        frame[pos++] = i;
                } else {
                        pos = 0;
                        if (!wait) {
                                return -1;
                        }
                }
        }

        return 0;
}
