#ifndef __COMPILER_H
#define __COMPILER_H

#ifndef inline
#define inline __inline
#endif

#ifndef asm 
#define asm __asm
#endif

#ifndef __aligned
#define __aligned(n)    __attribute__((aligned(n)))
#endif

#ifndef __unused
#define __unused__      __attribute__((unused))
#endif

#ifndef offsetof
#define offsetof(type, member) __builtin_offsetof(type, member)
#endif

#define __io            volatile
#define __flash__       __attribute__((section (".USER_FLASH")))
#define __init__        __attribute__((section (".init")))
#define __packed__      __attribute__((__packed__))
#define __deprecated__  __attribute__((__deprecated__))
#define __weak__        __attribute__((weak))

#endif /* __COMPILER_H */
