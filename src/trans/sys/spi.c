#include <avr/io.h>

#include "sys/spi.h"

void
spi_init(void)
{
        /* MOSI and SCK as output */
        DDRB |= _BV(MOSI) | _BV(SCK) | _BV(SS);


        /* enable SPI as master, SCK = F_OSC/64 */
        SPCR = _BV(SPE) | _BV(MSTR) | _BV(SPR0);
}

void
spi_send_simple(uint8_t c)
{
        /* put SS low */
        PORTB &= ~_BV(SS);

        SPDR = c;

        /* wait until data has been sent */
        while (!(SPSR & (1 << SPIF)))
                ;

        /* put SS high */
        PORTB |= _BV(SS);
}

void
spi_send_command(uint8_t c, uint8_t d)
{
        /* put SS low */
        PORTB &= ~_BV(SS);

        SPDR = c;

        /* wait until data has been sent */
        while (!(SPSR & (1 << SPIF)))
                ;

        SPDR = d;

        /* wait until data has been sent */
        while (!(SPSR & (1 << SPIF)))
                ;

        /* put SS high */
        PORTB |= _BV(SS);
}

void
spi_disable(void)
{
        SPCR &= ~_BV(SPE);
}
