# Olimexino-STM32

The Olimexino board is responsible for receiving the data and displaying
it on a serial LCD.

Directories:

    app         <-- main app
    include     <-- include files from app/ and sys/
    ldscripts   <-- linker script
    libs        <-- CMSIS and STD Peripheral Lib
    Makefile
    newlib      <-- newlib syscalls and startup file (_start)
    sys         <-- interrupt vectors, clock setup and MCU init
    scripts     <-- upload script and configuration file for openocd

The main loop is responsible for retrieving the data over UART, while a
general timer is responsible for updating the local clock and system uptime.

## External libraries and dependecies

### CMSIS and Std Peripheral Library

For using the peripherals there are plenty of alternatives.
[Arduino](http://arduino.cc/en/Main/Software) clone libraries like
[libmaple](https://github.com/leaflabs/libmaple) are among the most popular
today. 

These libraries, although useful have a lot of cruft not needed for
this case. So, instead of that, I've decided to use the [CMSIS][12] and 
[Standard Peripheral Library][13]. These are already bundled and enabled. In case a
different board/MCU must be used, the model version and the standard peripheral
library have to be enabled in `libs/CMSIS/CM3/DeviceSupport/ST/STM32F10x/stm32f10x.h`.  
I've already done this with help of the `Makefile` by passing directly the
macros to the compiler. You'll definitively need to modify the model to suite
your needs.

### Newlib and startup, and interrupt vectors

[newlib][15] is a embedded library useful in making use of printing functionality and 
memory allocation among other things. Also it defines the entry point of the 
main program. Check out [entry.S](src/recv/newlib/entry.S) for more details.

## Receiving and sending data over UART

The first and the second UART lines are initialized to default configuration
values: 8-bit data, no parity bit, and 1-stop bit. 
The speed rates for using the UART to display the data on the LCD is the default
(9600B) one, while for receiving data it matches the one from the sender
(1200B). Both UART lines have interrupts enabled. A simple ring buffer structure
has been implemented for storing the bytes received over the line. See
[interrupt file](src/recv/sys/stm32f10x_it.c) for more details.

### Sending data

Sending data to the LCD is achieved using the newlib `printf()` method, by
implementing [syscalls](src/recv/newlib/syscalls.c) and defining a proper
uart method for sending, which are implemented in [usart.c](src/recv/app/usart.c). 
Due to the implementation nature of `printf()`, it is required to disable buffering 
of data. Normally `printf()` will buffer the data until it encounters a `\n` (new
line) -- this is achieved using `setvbuf()` method, and it takes place in the `setup()`
method in [main](src/recv/app/main.c). 

Also, the LCD has some special commands: 
* brightness
* cursor positionw 
* clearing the entire display and so on. 

Noticed that is much more helpful to clear the entire display instead of
adjusting/matching the lenght of the string.  Additional info can be found in
[src](src/recv/app/lcd-usart.c).

### Receiving data

Receiving is done by retrieving a byte from the ring buffer, after verifying
that is indeed available. The same methods are defined in [same
file](src/recv/app/usart.c). See [RF](doc/RF_PROTO.md) part for details
regarding re-constructing the data from the transmitter.

## Timer interrupt

`TIM2` is a general timer in output compare mode which overflows every seconds.
A callback method is given when configuring the timer that will get called in
the interrupt timer handler. The callback method allows to update the uptime and
local clock. The uptime makes uses of the sys-tick facility of the Cortex-M3,
while for keeping track of the time, the RTC is being initialized and used
before initializing the timer.

[12]: http://www.arm.com/products/processors/cortex-m/cortex-microcontroller-software-interface-standard.php
[13]: http://www.st.com/web/catalog/tools/FM147/CL1794/SC961/SS1743/LN1734/PF257890
[15]: https://sourceware.org/newlib/
