RF Weather station
==================

### Description

A weather station capable of sending its data over RF.

---

This can be adapted to different hardware and needs. Some of the displays serve
only purely informational requirements. The hardware (at least on the receiver
side) is above the requirements but that's the only thing I could've spared
at the moment.

## Hardware and functionality

### Transmitter 

* [BeagleboardTrainer-xM][2] with [ATMEGA328p][10] -- and I/O board for BeagleBone board.
    * responsible for retrieving temperature and pressure from a sensor.
    * transmits periodically data over RF, hooked up to the serial line.
    * displays the temperature and the current time on a 7-segment display.

### Receiver 

* [Olimexino-STM32][1] w/ Cortex-M3 [STM32F103RBT6][11] -- a board with a lot of 
features and the ability to use its expansion port other things, like Ethernet.
    * responsible for receiving data over RF, hooked up on the second serial line of
the Olimexino board.
    * displays the temperature/pressure, the current time and the uptime to a LCD
hooked up on the first serial line.

### Temperature/Pressure Sensor

* [Bosch BMP085][7], Temperature/Pressure sensor, communicates
over I2C with the ATMEGA328p. The I2C protocol states that that line doesn't go *high*,
so 2 (two) 2-10kOhm pull-up rezistors are required for data (SDA) and clock (SCL) line. 
I've used 4k7Ohm for this purpose.

### Display

* [7-Segment Display][9] -- connected to the SPI interface
to display data on the transmitter.
* [SerLCD][6] -- connected to the first serial interface 
to display temp/pressure at the receiver.

### RF modules

* [XY-FST][8] RF-Transmitter
    * Operating voltage: 3,5 - 12V DC
    * Range: 20 - 200m (Depending on the voltage supplied) 
    * Modulation: AM 
    * Rate of transmission: 4KB/s 
    * Frequency: 433Mhz 
* [XY-MK][8] RF-Receiver
    * Operating voltage: 5V DC
    * Frequency: 433Mhz 

There aren't any *official* data sheets or specifications, but in order to
function properly the receiver must be hooked to an **independent** 5V power
supply.

Secondly, both modules have a `ANT` hole, and a lot of people suggested that
a copper wire will **have** to be attached/soldered.  Being over-zealous I've
soldered a 4-cm copper wire -- which is huge and most important very heavy.
This is also over-kill for my range (under 100m).

#### Sending data over UART

The last point is that I've used the serial line to send and receive data.
This is actually tricker that I thought. The line speed must be somewhere
between 900Bauds and 2400Bauds and the data must be encapsulated/encoded so
that the receiver can make sense of what actually is being sent. Did some basic
tests: hooked up to the PC serial line showed that when no data is being sent
there's *a lot* of noise/interference, although when sending data continuously 
things improve considerably. See [RF protocol](doc/RF_PROTO.md) for more information.

### Burning/Uploading code

**Please note that this will erase the code currently running on the MCUs**.

Both the ATMEGA328p and the Cortex-M3 are capable of performing some kind of
booting either over the serial line or, in the case of Cortex-M3, over USB using 
DFU specification. Given that I used/wanted a home-made solution, I didn't 
bother with this approach, and instead opted to upload the code using 
ISP and JTAG interface. For this some operation some kind of dedicated hardware 
is required.

Some useful and cheap alternatives to the ones provided by the MCU manufacturer
which I use are:

* [AVR-ISP500][5] for uploading code to ATMEGA328p
* [ARM-USB-OCD-H][4] for uploading code to STM32F103RB

In the event of maintaining the boot-loader you will have to adjust/adapt
the linker scripts that detail the memory map and enable the `VECT_TAB_ADDR`
defined macro for Cortex-M3, while for ATMEGA will you need to pass
to the linker the entry address over the command line.

## Block diagram

A rather crude block diagram is included. The Trainer board already has the
hardware required to enable USART TTL, and to support I2C and SPI interfaces.
In case you use your own PCB design  additional hardware components will be
required. There are plenty of schematics which show what you might need. For
instance, the [Trainer schematic][2] is useful in understanding what
additional parts are needed.

                                                                   Vcc 5V          Vcc 3.3V                                       
           +-------------+-------+----------------------------+-----+                   +--------------+-------------------------+
           |             |       |                            |                                        |                         |
           |             |       |                            |                                        |                         |
           +--+---+      |       |                            |  Antenna        Antenna      Vcc 5V    |                         |
           |  |   |      |       |                            |  +                    +   +-----+      |                         |
           | +++ +++     |   +---+----------------+           |  |                    |   |            |                         |
           | | | | |     |   |  AVcc              |           |  |                    |   |            |                         |
           | | | | |4k7O |   |                    |      PWR  |  |                    |   |PWR         |                         |
           | +++ +++     |   |                    |    +------+--+-+                +-+---+-----+      |                         |
           |  |   |      |   |                  TX+----+           |                |           +-+    |                         |
           |  |   |      |   |                    |    |  XY+FST   |                |   XY+MK   | |    |                         |
           |  +--------------+SDA                 |    |           |                |           | |    |                         |
           |  |   |      |   |                    |    +------+----+                +-----+-----+ |  +-+----------------------+  |
           |  |   +----------+SCL                 |           |GND                        | GND   |  |                        |  |
        PWR|  |   |      |   |                    |           |                           |       |  |                        |  |
         +-+--+---+--+   |   |     Trainer+xM     |  +----+   |                           |       |  |        Olimexino       |  |
         |           |   |   |     ATMEGA328p   D2+--+BUT0+---+                           |       +--+RX2    STM32F103RB      |  |
         |   BMP085  |   |   |                    |  +----+   |                           |          |                        |  |
         |           |   |   |                    |  +----+   |                           |          |                        |  |
         +------+----+   |   |                  D3+--+BUT1+---+                           |        +-+GND  TX1                |  |
                |GND     |   |                    |  +----+   |                           |        | +------+-----------------+  |
                |        |   |                    |  +----+   |                           |        |        |                    |
                |        |   |                  D4+--+BUT2+---+                           |        |        | +------------------+
                |        |   |                    |  +----+   |                           |        |        | |PWR               |
                |        |   |                    |           |                           |        |    +---+-+-----------+      |
                |        |   |   MOSI  SS SCK  GND|           |                           |        |    |      SerLCD     |      |
                |        |   +------+---+---+---+-+           |                           |        |    +---+-------------+      |
                |        |          |   |   |   |             |                           |        |        |GND                 |
                |        |    PWR +-+---+---+-+ |             |                           |        |        |                    |
                |        +--------+ 7 segment | |             |                           |        |        |                    |
                |                 |  display  | |             |                           |        |        |                    |
                |                 +-----+-----+ |             |                           |        |        |                    |
                |                       |GND    |             |     GND             GND   |        |        |                    |
                +-----------------------+-------+-------------+------+               +----+--------+--------+--------------------+


In case of ARM, each hardware implementation provides the necessary 
hardware components and interfaces in order to function out of the box.

## Building and burning

### Requirements

* gnu make
* [avr-libc](http://www.nongnu.org/avr-libc/), gcc-avr, binutils-avr for ATMEGA328p/Trainer
* [arm-none-eabi-gcc](https://launchpad.net/gcc-arm-embedded) and [newlib][15]
for Cortex-M3/Olimexino.
* [openocd](http://openocd.sourceforge.net/) and [avrdude](http://www.nongnu.org/avrdude/)
for burning the code into flash memory of the MCU.

Various GNU/Linux distributions have cross-compiled tool-chains packaged which 
can be easily installed.

### Building and burning

The receiver and the transmitter have their either own separate 
source directories. In case you're using different programmers or MCUs will 
have to adjust the configuration files for either avrdude or openocd.

To upload the code for each part just issue a:

    $ make burn 

You can enable the `SYSTEM_DATE` option as to pass the initial clock value
to the MCU/board. For the trainer board I've hooked a couple of buttons to 
allow a manual configuration, but the Olimexino has only one button and was to
lazy to implement something for it. This is a cheap trick to bypass that. Just
issue a:

    $ make clean && make SYSTEM_DATE=1 burn

to use your systems clock as a time clock.

#### Additional information

* *ATMEGA328p* a more detailed description of the state machines running on the
board can be found [here](doc/TRANS.md).
* *Olimexino* -- the [same](doc/RECV.md), but for Cortex-M3 board.

## Resources

### ATMEGA

* [Interrupt driven USARTs](http://www.avrfreaks.net/forum/tut-soft-using-usart-interrupt-driven-serial-comms?page=all)
* [AVR Articles](http://www.fourwalledcubicle.com/AVRArticles.php)

### STM32F103RB

* [Using STM32 StdLib](http://pandafruits.com/stm32_primer/stm32_primer_lib.php)

### Basic overview of protocols

* I2C https://learn.sparkfun.com/tutorials/i2c
* UART https://learn.sparkfun.com/tutorials/serial-communication
* SPI https://learn.sparkfun.com/tutorials/serial-peripheral-interface-spi

[1]: https://www.olimex.com/Products/Duino/STM32/OLIMEXINO-STM32/resources/OLIMEXINO-STM32.pdf
[2]: https://www.tincantools.com/_newsite2013/userfiles/file/trainer_rev-b.pdf
[3]: http://elinux.org/BeagleBoard_Trainer
[4]: https://www.olimex.com/Products/ARM/JTAG/_resources/ARM-USB-OCD_and_OCD_H_manual.pdf
[5]: https://www.olimex.com/Products/AVR/Programmers/AVR-ISP500/resources/AVR-ISP500.pdf
[6]: https://www.sparkfun.com/datasheets/LCD/SerLCD_V2_5.PDF
[7]: https://www.adafruit.com/datasheets/BMP085_DataSheet_Rev.1.0_01July2008.pdf
[8]: http://www.amazon.de/Empf%C3%A4nger-Superregeneration-Raspberry-Wireless-Transmitter-433-MHz-Funk-Sende-Modul-f%C3%BCr-Arduino/dp/B00M0XTP4W/ref=lh_ni_t?ie=UTF8&psc=1&smid=A2ZIVEMH0WUCWD
[9]: https://www.sparkfun.com/datasheets/Components/LED/7-Segment/SFE-0012-DS-7segmentSerial-v41.pdf
[10]: http://www.atmel.com/images/Atmel-8271-8-bit-AVR-Microcontroller-ATmega48A-48PA-88A-88PA-168A-168PA-328-328P_datasheet_Complete.pdf
[11]: http://www.st.com/web/en/resource/technical/document/datasheet/CD00161566.pdf
[12]: http://www.arm.com/products/processors/cortex-m/cortex-microcontroller-software-interface-standard.php
[13]: http://www.st.com/web/catalog/tools/FM147/CL1794/SC961/SS1743/LN1734/PF257890
[14]: http://www.atmel.com/webdoc/AVRLibcReferenceManual/index.html
[15]: https://sourceware.org/newlib/
