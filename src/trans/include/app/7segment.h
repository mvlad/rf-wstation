#ifndef __SEVEN_SEGMENT_H
#define __SEVEN_SEGMENT_H

#define SEVEN_SEGMENT_COMMAND_BRIGHTNESS        0x7a    /* 'z' */
#define SEVEN_SEGMENT_COMMAND_DECIMALS          0x77    /* 'w' */ 
#define SEVEN_SEGMENT_COMMAND_CLEAR             0x76    /* 'v' */

#define SPECIAL_SINGLE_CHAR_A                   0x7b    /* control digit 1 */
#define SPECIAL_SINGLE_CHAR_B                   0x7c    /* control digit 2 */
#define SPECIAL_SINGLE_CHAR_C                   0x7d    /* control digit 3 */
#define SPECIAL_SINGLE_CHAR_D                   0x7e    /* control digit 4 */

struct clock {
        uint8_t hrs;
        uint8_t mins;
        uint8_t secs;
};

/*
 * spi interface
 *
 * 7segment <-> ATMEGA328p
 *
 * Connect CSN to D10 (PINB2)
 * Connect MOSI to SI (PINB3)
 * Conect  SCK to SCK (PINB5)
 */
extern void 
ss_set_brightness(uint8_t val);

extern void 
ss_clear_display(void);

extern void 
ss_set_decimals(uint8_t val);

extern void 
ss_send_str(char *str);

extern void
ss_setup(void);

extern void
ss_write_no(uint32_t no);

extern void
ss_write_data(int8_t quot, int8_t rem);

extern void
ss_write_date(volatile struct clock *d);

#endif
